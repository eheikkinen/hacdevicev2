#include <EEPROM.h>
#include <SPI.h>
#include <Adafruit_CC3000.h>
#include <CC3000_MDNS.h>
#include <Wire.h>
#include <RTClib.h>
#include <ArduinoJson.h>

#include "HAL.h"
#include "ModuleRestApi2.h"
#include "EepromHelper.h"
#include "RobustWifi.h"
#include "Device.h"
#include "RelayModule.h"
#include "TimerModule.h"
#include "BrightnessModule.h"
#include "DhtSensorModule.h"
#include "utility/debug.h" // getFreeRam() method

#define DEBUG_MODE 1

#ifndef DEBUG_MODE
#define DEBUG_MODE 0
#endif

// Library classes
Adafruit_CC3000 _cc3000         = Adafruit_CC3000( ADAFRUIT_CC3000_CS, ADAFRUIT_CC3000_IRQ, ADAFRUIT_CC3000_VBAT, SPI_CLOCK_DIV2 );
Adafruit_CC3000_Server          _httpServer( LISTEN_PORT );
MDNSResponder                   _mdns;
RTC_DS1307                      _rtc;

// Own classes
EepromHelper _eepromHelper          = EepromHelper( EEPROM );
Device _device                      = Device( &_eepromHelper, LED_PIN );
ModuleRestApi2 _moduleRestApi       = ModuleRestApi2( &_device );
RobustWifi _robustWifi              = RobustWifi( &_eepromHelper, &_cc3000, &_httpServer, &_mdns, &_moduleRestApi, &_device );

RelayModule _relayModule            = RelayModule( &_eepromHelper, 0, 8 );          // Eemprom memory index 0, pin number digital 8
TimerModule _timerModule            = TimerModule( &_eepromHelper, 1, 8 );          // Eemprom memory index 1, pin number digital 8
BrightnessModule _brightnessModule  = BrightnessModule( &_eepromHelper, 2, A0 );    // Eemprom memory index 2, pin number analog A0
DhtSensorModule _dhtModule          = DhtSensorModule( &_eepromHelper, 3, 6 );      // Eemprom memory index 3, pin number digital 6
BaseModule* _modules[MODULE_COUNT]  = { &_relayModule, &_timerModule, &_brightnessModule, &_dhtModule };


// General variables
unsigned char _5msInterrupt                     = false;
unsigned char _secondInterrupt                  = false;
unsigned char _30secondsInterrupt               = false;
unsigned int _interruptCounter                  = 0; // Incremented every 5ms
volatile unsigned char _restartSmartConfig      = false;
volatile unsigned long _lastPushButtonPressMs   = 0;
volatile unsigned char _firstPushButtonInterrupt = true; // Blocks the first push button ISR, it is triggered when initialized!


void setup(void)
{
    Serial.begin( 115200 );
    Serial.println("Board starting");
    
    initializeRtc();
    initializeTimer1();
    
    // Pin initialization
    pinMode( BUTTON_PIN, INPUT_PULLUP );
    attachInterrupt( 0, pushButtonISR, FALLING ); // 0 = interrupt 0 => digital pin 2
    
    _robustWifi.initialize();
    
    // Initialize modules
    for( uint8_t i=0; i<MODULE_COUNT; i++ )
        _modules[i]->initialize();
        
    _robustWifi.addModules( _modules );
    
    Serial.println(F("Initializations done"));
}

/**
 * 5 ms intervals
 */
ISR( TIMER1_COMPA_vect )
{
    _5msInterrupt = true;
    
    _interruptCounter++;
    
    _device.handleLed( _interruptCounter );
    
    if( _interruptCounter >= 12000 ) // 60 seconds
        _interruptCounter = 0;

    if( _interruptCounter == 6000 || _interruptCounter == 0 ) // 30 seconds
        _30secondsInterrupt = true;
        
    if( _interruptCounter % 200 == 0 )
        _secondInterrupt = true;
}

/************************************************************************/
/* Push button interrupt service routine                                */
/************************************************************************/
void pushButtonISR( void )
{
    // Block the first ISR that is triggered by the initialization of the pullup resistor
    if( _firstPushButtonInterrupt ) {
        _firstPushButtonInterrupt = false;
        return;
    }
    
    unsigned long currentMs = millis();
    
    // Ignore bounces for 1000ms when press starts
    if( ( _lastPushButtonPressMs + 1000 ) > currentMs )
        return;
    
    _lastPushButtonPressMs = currentMs;
    
    _robustWifi.pushButtonPressed();
}

void loop()
{
    if( _5msInterrupt )
        handleInterrupts();
    
    // < TODO > This if blocking in error situations!
    _robustWifi.handle(); // Updates RSSI every 30 seconds
}


void handleInterrupts( void )
{
    // Once per second
    if( _secondInterrupt ) {
        //Serial.print("Second interrupt"); Serial.println(_interruptCounter);
        // Check if timer should toggle something
        _timerModule.handleSecondPoll( &_rtc.now() );
        
        _brightnessModule.handleSecondPoll();
        
        _secondInterrupt = false;
    }
    
    // Once per 30 seconds
    if( _30secondsInterrupt ) {
        _robustWifi.handleReconnectIfNothingReceived();
        _robustWifi.updateWifiRssi(); // This is slow!
        _30secondsInterrupt = false;
    }
    
    _5msInterrupt = false;
}

void initializeTimer1( void )
{
    cli(); // Disable interrupts
    
    TCCR1A  = 0; // Empty timer time
    TCCR1B  = 0; // Empty timer time
    TCNT1   = 0; // Initialize counter value to 0
    OCR1A   = 312; // ~5 ms
    TCCR1B  |= (1 << CS12); // 256 prescalar
    TCCR1B  |= (1 << WGM12); // Clock compare trigger mode
    TIMSK1  |= (1 << OCIE1A); // Enable t1 interrupt
    
    sei(); // Enable interrupts
}
/*
void initializeModules( void )
{
    Serial.println(F("Initializing modules"));
    
    
    if( !_eepromHelper.saveEepromTimerModuleRow( _timerModuleNumber, 0, 7, 20, 00, 1 ) )
        Serial.println(F("Failed to write timer module row to eeprom!"));
    else
        Serial.println(F("Succesfully wrote timer module data to eeprom"));
    
    
    _relayModule.initialize();
    _timerModule.initialize();
    
}
*/
    
void initializeRtc()
{
    Wire.begin();
    _rtc.begin();
    _rtc.adjust( DateTime(F(__DATE__), F(__TIME__)) );
}



