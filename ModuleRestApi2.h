
#ifndef _MODULERESTAPI2_H_
#define _MODULERESTAPI2_H_

#include "Arduino.h"
#include <Adafruit_CC3000.h>
#include "HAL.h"
#include "BaseModule.h"
#include "Device.h"

//#define MODULE_REST_API_DEBUG

// Own defines
#define MODULERESTAPI2_INPUT_BUFFER_SIZE 75
#define MODULERESTAPI2_OUTPUT_BUFFER_SIZE 600 // Headers: 181, TimerModule data with 10 rows MAX 410 = 591

class ModuleRestApi2
{
public:
    ModuleRestApi2( Device* device );
    uint8_t handle( Adafruit_CC3000_ClientRef &client );
    void addModule( BaseModule* module );
    void addModules( BaseModule* modules[MODULE_COUNT] );
    uint8_t getPacketReceived();
    
private:
    // Private methods
    uint8_t processReceivedData( Adafruit_CC3000_ClientRef &client );
    uint8_t processAcquiredData( char c );
    uint8_t validateAndExecuteReceivedData( void );
    void buildBadRequestAnswer( void );
    void buildSuccessAnswer( uint8_t id );
    void sendResponse( Adafruit_CC3000_ClientRef &client, uint8_t chunkSize, uint8_t wait );
    void resetStatus( void );
    void addToBuffer(int toAdd);
    void addToBuffer( char* toAdd );
    void addToBuffer( const __FlashStringHelper* toAdd );
    uint16_t getIntFromAsciiAnswer( void );
    
    // Private variables
    Device*     _device;
    
    char        _answer[MODULERESTAPI2_INPUT_BUFFER_SIZE];
    uint8_t     _answerIndex;
    
    uint8_t     _method; // g get, p post, u put, d delete
    uint8_t     _command;
    uint16_t    _moduleValue;
    uint8_t     _packetReceived;
    
    uint8_t     _moduleIndex;
    uint8_t     _moduleSelected;
    uint8_t     _urlEndVerified;
    uint8_t     _answerIsError;
    
    uint8_t     _modulesAdded;
    
    char        _outputBuffer[MODULERESTAPI2_OUTPUT_BUFFER_SIZE];
    uint16_t    _outputBufferIndex;
    
    //char      _deviceName[DEVICE_NAME_LENGTH];
    //char      _humanDeviceName[HUMAN_NAME_LENGTH];
    //uint8_t   _macAddressString[19];
    //uint8_t   _rssi;
    
    BaseModule* _modules[MODULE_COUNT];
};

#endif
