#ifndef _ROBUSTWIFI_H_
#define _ROBUSTWIFI_H_

//#define ROBUST_WIFI_DEBUG


#include "Arduino.h"
#include <SPI.h>
#include <Adafruit_CC3000.h>
#include <CC3000_MDNS.h>

#include "HAL.h"
#include "EepromHelper.h"
#include "Device.h"
#include "ModuleRestApi2.h"
#include "BaseModule.h"

class RobustWifi
{
public:
    RobustWifi( EepromHelper* eepromHelper, Adafruit_CC3000* cc3000, Adafruit_CC3000_Server* server, MDNSResponder* mdns, ModuleRestApi2* moduleRestApi, Device* device );

    void initialize();
    void handle();
    uint8_t isConnected();
    void pushButtonPressed();
    uint8_t getBlinkRate();
    void handleReconnectIfNothingReceived();
    void addModule( BaseModule* module );
    void addModules( BaseModule* modules[MODULE_COUNT] );
    void updateWifiRssi();
	
private:
    void bootupOpenWifiUsingSmartConfs();
    void bootupRestartWifiSmartConfig();
    void getDeviceNameFromMacAddress();
    void retryOpenWifi();
    uint8_t retryOpenWifiUsingSmartConfig();
    void getConnectionDetails();

    EepromHelper*           _eepromHelper;
    Adafruit_CC3000*        _cc3000;
    Adafruit_CC3000_Server* _server;
    MDNSResponder*          _mdns;
    ModuleRestApi2*         _moduleRestApi;
    Device*                 _device;

    char _ssid[33];

    volatile uint8_t _pushButtonPressed;
    volatile uint8_t _blinkRate;
};

#endif
