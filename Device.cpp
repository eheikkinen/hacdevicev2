#include "Device.h"

/************************************************************************/
/*                          PUBLIC METHODS                              */
/************************************************************************/

Device::Device( EepromHelper* eepromHelper, uint8_t ledPin )
{
    _rssi               = 0;
    _eepromHelper       = eepromHelper;
    _blinkRate          = BLINK_RATE_2HZ;
    _ledPin             = ledPin;
    _ledValue           = 1;
    _keepLedOffCounter  = 0; // After receiving packet, this counter keeps led off until zeroed
    
    pinMode( _ledPin, OUTPUT );
    
    // Initialize device name and human device name
    _deviceNameSet = _eepromHelper->getDeviceNames( _deviceName, _humanDeviceName );
    
    _deviceName[DEVICE_NAME_LENGTH-1]       = 0;
    _humanDeviceName[HUMAN_NAME_LENGTH-1]   = 0;
}

/**
 * @brief Device::setDeviceNameFromMacAddress
 * Sets and saves the device name from the Wifi module mac address
 *
 * @param macAddress The Wifi module mac address
 */
void Device::setDeviceNameFromMacAddress( uint8_t macAddress[6] )
{
    char* buf_ptr = (char*)_deviceName;
    
    memset( _deviceName, 0, sizeof( _deviceName ) );
    strcpy( _deviceName, NAME_PREFIX );
    buf_ptr = buf_ptr + 4;
    for( uint8_t i=3; i<6; i++) { // Ignore first 3 bytes of mac
        if( i != 3 )
            buf_ptr += sprintf( buf_ptr, "-" );
        
        buf_ptr += sprintf( buf_ptr, "%02X", macAddress[i] );
    }
    *(buf_ptr + 1) = '\0';
    
    // Lower case everything
    for( uint8_t i=0; i<DEVICE_NAME_LENGTH; ++i)
        _deviceName[i] = tolower( _deviceName[i] );
    
#ifdef DEVICE_DEBUG
    Serial.print("Mac address -> device name: "); Serial.println( _deviceName );
#endif
    
    _eepromHelper->saveEepromDeviceName( _deviceName );
}

/**
 * @brief Device::getDeviceNamePtr
 * Getter for the device name pointer
 *
 * @return The device name char pointer
 */
char* Device::getDeviceNamePtr()
{
    return (char*)&_deviceName;
}

/**
 * @brief Device::getHumanDeviceNamePtr
 * Getter for the human device name pointer
 *
 * @return The human device name char pointer
 */
char* Device::getHumanDeviceNamePtr()
{
    return (char*)_humanDeviceName;
}

/**
 * @brief Device::setHumanDeviceName
 * Sets and saves the device human to memory and eeprom
 *
 * @param humanDeviceName   The human device name
 * @param length            The length of the device name
 */
void Device::setHumanDeviceName( char* humanDeviceName, uint8_t length )
{
    if( length > HUMAN_NAME_LENGTH-1 )
        length = HUMAN_NAME_LENGTH-1;
    
    memcpy( _humanDeviceName, humanDeviceName, length );
    _humanDeviceName[length] = 0; // Make sure last char is 0
    _eepromHelper->saveEepromHumanDeviceName( _humanDeviceName );

#ifdef DEVICE_DEBUG
    Serial.print("Saved human name: "); Serial.println(_humanDeviceName);
#endif
}

/**
 * @brief Device::getRssi
 * Getter for the Wifi RSSI (signal strength)
 *
 * @return The last RSSI
 */
uint8_t Device::getRssi()
{
    return _rssi;
}

/**
 * @brief Device::setRssi
 * Setter for the RSSI (signal strength)
 *
 * @param rssi The value to set
 */
void Device::setRssi( uint8_t rssi )
{
    _rssi = rssi;
}

/**
 * @brief Device::isDeviceNameSet
 * Helper for checking if the device name is already set
 *
 * @return True if set, false if not set
 */
uint8_t Device::isDeviceNameSet()
{
    return _deviceNameSet;
}

/**
 * @brief Device::initiateBlinkRate
 * Helper for initiating the led blinking with 1Hz
 */
void Device::initiateBlinkRate()
{
    _blinkRate = BLINK_RATE_1HZ;
}

/**
 * @brief Device::getBlinkRate
 * Getter for the current blinking rate of the led
 *
 * @return The current blink rate in milliseconds
 */
uint8_t Device::getBlinkRate()
{
    return _blinkRate;
}

/**
 * @brief Device::setBlinkRate
 * Setter for the blink rate
 *
 * @param rate The blink rate value in milliseconds
 */
void Device::setBlinkRate( uint8_t rate )
{
#ifdef DEVICE_DEBUG
    Serial.print("Blink rate to: "); Serial.println( rate );
#endif
    _blinkRate = rate;
}

/**
 * @brief Device::handleLed
 * Some debug helper
 *
 * @param counter
 */
void Device::handleLed( uint16_t counter )
{
    if( _keepLedOffCounter && _keepLedOffCounter-- )
        return;
    
    if( _blinkRate == 0 )
        digitalWrite( _ledPin, true );
    else if ( counter % _blinkRate == 0 ) {
        digitalWrite( _ledPin, _ledValue );
        _ledValue = !_ledValue;
    }
}

/**
 * @brief Device::turnOffLed
 * Method for forcing the LED off
 */
void Device::turnOffLed()
{
    digitalWrite( _ledPin, 0 );
    _keepLedOffCounter = 10; // 100ms
}

/**
 * Updates the device human_name
 *
 * Payload:
 *   {"human_name":"Living room"}
 */
uint8_t Device::restfulUpdate( char buffer[MODULE_BUFFER_SIZE] )
{
    StaticJsonBuffer<MODULE_BUFFER_SIZE> jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject( buffer );
    
    if( !root.success() ) {
        memset( &buffer[0], 0, MODULE_BUFFER_SIZE );
        strcpy( buffer, "Invalid json" );
        return false;
    }
    
    const char* humanName = root["human_name"];
    setHumanDeviceName( (char*)humanName, strlen(humanName) );
    
    return true;
}


/************************************************************************/
/*                          PRIVATE METHODS                             */
/************************************************************************/



