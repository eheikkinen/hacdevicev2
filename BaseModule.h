
#ifndef _BASEMODULE_H_
#define _BASEMODULE_H_

#include "Arduino.h"
#include "HAL.h"
#include "EepromHelper.h"   // EepromHelper class

//#define MODULE_BASE_DEBUG 1

class BaseModule
{
public:
    BaseModule( EepromHelper* eepromHelper, uint8_t eepromIndex, uint8_t pin, uint8_t pinType = 'd' );
    
    // General module methods
    void getModuleType( char moduleType[4] );
    uint8_t isInitialized();
    uint8_t isEepromSaveable();
    uint8_t getPin();
    uint8_t getPinType();
    uint8_t write( uint16_t value );
    int16_t read();
    
    // Module specific methods
    virtual void initialize() = 0;
    virtual uint8_t readStatus( char buffer[MODULE_BUFFER_SIZE] ) = 0;
    
    // CRUD methods
    virtual uint8_t restfulCreate( char buffer[MODULE_BUFFER_SIZE] );   // POST
    virtual uint8_t restfulRead( char buffer[MODULE_BUFFER_SIZE] ) = 0; // GET
    virtual uint8_t restfulUpdate( char buffer[MODULE_BUFFER_SIZE] );   // PUT
    virtual uint8_t restfulDelete( char buffer[MODULE_BUFFER_SIZE] );   // DELETE

protected:
    
    uint8_t addModuleJsonIdToBuffer( char buffer[] );
    int16_t validateValue( const char* value );

    uint8_t _pin;
    uint8_t _pinType;
    uint8_t _eepromIndex;
    uint8_t _eepromUsage;
    uint8_t _initialized;
    char    _moduleType[4];
    uint8_t _jsonChuckCounter;
    
    uint8_t _readable;
    uint8_t _writable;
    uint8_t _eepromSaveable;
    
    uint16_t_currentValue;
    
    EepromHelper* _eepromHelper;
};

#endif
