#include "RelayModule.h"

RelayModule::RelayModule( EepromHelper* eepromHelper, uint8_t eepromIndex,uint8_t pin, uint8_t pinType ) :
    BaseModule( eepromHelper, eepromIndex, pin, pinType )
{
}

/**
 * Initialize 3 steps:
 * - Pins
 * - The four variables
 * - Initial values
 */
void RelayModule::initialize()
{
    // Pins
    pinMode( _pin, OUTPUT );
    digitalWrite( _pin, LOW );
    
    // The four variables
    _readable       = true;
    _writable       = true;
    _eepromSaveable = false;
    strncpy( _moduleType, MODULE_RELAY, 4 );
    
    // Values
    // -
    
    // Done
    _initialized = true;

#ifdef MODULE_RELAY_DEBUG
    Serial.println(F("Relay module initialized"));
#endif
}

/**
 * Reads the relay module status in json format to the buffer
 *
 * Format:
 *  {"id"1,"type":"rel","value":1}
 */
uint8_t RelayModule::readStatus( char buffer[MODULE_BUFFER_SIZE] )
{
    if( !_initialized )
        return false;
    
    memset( &buffer[0], 0, MODULE_BUFFER_SIZE );
    
    // Relay must always get current value when querying, since many modules can control it
    read();
    
    uint8_t bufferPtr = 0;
    char number[5];

    // Add id: '{"id":1' (note no comma in end)
    bufferPtr = addModuleJsonIdToBuffer( buffer );
    
    // Add type
    strncpy( &buffer[bufferPtr], ",\"type\":\"", 9 );
    bufferPtr += 9;
    strncpy( &buffer[bufferPtr], _moduleType, 3 );
    bufferPtr += 3;
    
    // Add value
    strncpy( &buffer[bufferPtr], "\",\"value\":", 10 );
    bufferPtr += 10;
    
    itoa( _currentValue, number, 10 );
    for( uint8_t i=0; i< strlen( number ); i++ )
        buffer[bufferPtr++] = number[i];
    
    buffer[bufferPtr++] = '}';

#ifdef MODULE_RELAY_DEBUG
    Serial.print("RelayModule: latest status added bytes ");
    Serial.println(bufferPtr);
#endif
    
    return bufferPtr;
}


/************************************************************************/
/*                          RESTFUL METHODS                             */
/************************************************************************/

/**
 * Read relay value
 */
uint8_t RelayModule::restfulRead( char buffer[MODULE_BUFFER_SIZE] )
{
    if( !_initialized )
        return false;
    
    memset( &buffer[0], 0, MODULE_BUFFER_SIZE );
    
    // Relay must always get current value when querying, since many modules can control it
    read();
    
    if( _jsonChuckCounter == 0 ) {
        uint8_t bufferPtr = 0;
        char number[5];
        
        // Add type
        strncpy( &buffer[bufferPtr], ",\"type\":\"", 9 );
        bufferPtr += 9;
        strncpy( &buffer[bufferPtr], _moduleType, 3 );
        bufferPtr += 3;
        
        // Add value
        strncpy( &buffer[bufferPtr], "\",\"value\":", 10 );
        bufferPtr += 10;
        memset( number, 0, sizeof( number ) );
        itoa( _currentValue, number, 10 );
        for( uint8_t i=0; i< strlen( number ); i++ )
            buffer[bufferPtr++] = number[i];
        
        _jsonChuckCounter++;

#ifdef MODULE_RELAY_DEBUG
        Serial.print("RelayModule: restful read added bytes ");
        Serial.println(bufferPtr);
#endif
        
        return true;
    }
    
    _jsonChuckCounter = 0;
    return false;
}

/**
 * Update relay value
 *
 * Payload:
 *   { "value":"1" }
 */
uint8_t RelayModule::restfulUpdate( char buffer[MODULE_BUFFER_SIZE] )
{
    StaticJsonBuffer<MODULE_BUFFER_SIZE> jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject( buffer );
    
    if( !root.success() ) {
        memset( &buffer[0], 0, MODULE_BUFFER_SIZE );
        strcpy( buffer, "Invalid json" );
        return false;
    }
    
    int16_t value = validateValue( root["value"] );
    if( value == -1 ) {
        memset( &buffer[0], 0, MODULE_BUFFER_SIZE );
        strcpy( buffer, "Invalid value given" );
        return false;
    }
    
    write( value );
    
    return true;
}
