
// Module defines
#define MODULE_COUNT        4
#define MODULE_RELAY        "rel"
#define MODULE_CURRENT      "cur"
#define MODULE_TIMER        "tim"
#define MODULE_BRIGHTNESS   "bri"
#define MODULE_SENSOR_DHT   "tem"
#define MODULE_BUFFER_SIZE  60


// General settings
#define NAME_PREFIX             "dev-"
#define DEVICE_NAME_LENGTH      13 // Format: "dev-ff-22-4f" (12 + 0)
#define HUMAN_NAME_LENGTH       25 // Human name max length (24 + 0)

#define LED_PIN                 9
#define BUTTON_PIN              2

// CC3000 configurations
#define ADAFRUIT_CC3000_IRQ     3
#define ADAFRUIT_CC3000_VBAT    5
#define ADAFRUIT_CC3000_CS      10
#define LISTEN_PORT             80
#define RSSI_MAX_VALUE          83 // Used for scaling RSSI to 0-100

// Blink rates (multipliers for 5ms)
#define BLINK_RATE_1HZ          100 // 500 ms on, 500 ms off
#define BLINK_RATE_2HZ          50 // 250 ms on, 250 ms off
#define BLINK_RATE_ON           0

