### What is this repository for? ###

* This is a hobby project source codes for a Home Automation Controller. A modular device which can be remotely controlled and read via a Wifi connection using restful HTTP messages.

### Please note ###

* This is just a hobby project that evolved during coding. There was pretty much no planning beforehand.

### Example features & use cases ###

* Configure Wifi settings with an another Wifi capable device (TI Smartconfig, no need to do it compile time)
* Modular software design, easy to add new modules (e.g. sensor support)
* Toggle on/off lights at your home via Wifi
* Preconfigure (to eeprom) lights to turn on/off when average brightness of the device gets low/high enough
* Preconfigure (to eeprom) times when light should be toggled on or off via Wifi
* Read sensor data like temperature, humidity, brigtness or other (CO2 etc.) via Wifi
* Each device has a unique name generated from the Wifi mac address, but also supports naming the devices with a human readable string (e.g. "Living Room")
* EEPROM storing of the Wifi smartconfig, module data like timer values when to toggle led
* Sometimes the devices advertise themselves with MDNS (for an UI to automatically find devices without polling ips, doesn't work always :))

### Versions ###

* Version 2 ported for a more powerful Atmel ARM ATSAM3X8E controller
* Version 1 ported for an Atmel Atmega2560 controller with more memory
* Version 0 used Atmel Atmega32BP controller from the drawer

### Hardware parts ###

* Arduino Due developing board with Atmel ARM ATSAM3X8E
* Texas Instrumetns CC3000 Wifi (Adafruit board) + better external antenna
* Tiny RTC i2c board
* 1 Relay module for 230V 10A
* 230V-5V power adapter
* Temperature & humidity sensor with digital 1 wire output
* Brightness analog sensor
* Single LED
* Single push button
* 230 V plug (male -> female)
* Ikea [Montera](http://i335.photobucket.com/albums/m448/amroyo/bunny/bunnyproofing050.jpg) -plastic box as cover 

### Software used ###

* Atmel Studio 6.2
* Mainly C with some C++ syntax for the sake of clarity
* No C++ dynamic memory loading
+ External dependencies (Arduino libs)
* Adafruit CC3000 library
* CC3000 MDNS
* ArduinoJson
* EEPROM
* RTClib
* SPI
* Wire

### Backlog ###

* [Coding] Add more modules
* [Coding] Add support for MDNS (probably a CC3000 firmware bug)
* [Coding] Cleanup the RobustWifi codes
* [Coding] Fix the EEPROM class to separate files
* [Coding] Add more comments
* [Hardware] Figure out how to integrate the device (or a smaller one) in to a light switch for remote control (no ground)
* [Other] Create the JSON API documentation
* [Hardware] Draw electronic design from the proto device

### Who do I talk to? ###

* Repo owner

### Images ###

* Here is the HACv2 almost production ready
![HACv2.jpg](https://bitbucket.org/repo/oBa5A7/images/4104554327-HACv2.jpg)

* Here is the HACv0tov1
![HACv0tov1.jpg](https://bitbucket.org/repo/oBa5A7/images/1348648747-HACv0tov1.jpg)