
#ifndef _DHTSENSORMODULE_H_
#define _DHTSENSORMODULE_H_

#include "BaseModule.h"

//#define MODULE_DHTSENSOR_DEBUG 1

class DhtSensorModule : public BaseModule 
{
public:
    DhtSensorModule( EepromHelper* eepromHelper, uint8_t eepromIndex, uint8_t pin, uint8_t pinType = 'a' );
    void initialize();
    uint8_t readStatus( char buffer[MODULE_BUFFER_SIZE] );
    
    // Overrided CRUD methods
    uint8_t restfulRead( char buffer[MODULE_BUFFER_SIZE] );     // GET
    
private:
    uint8_t readDht();
    
    uint8_t _temperature;
    uint8_t _humidity;
};

#endif
