#include "BrightnessModule.h"

BrightnessModule::BrightnessModule( EepromHelper* eepromHelper, uint8_t eepromIndex, uint8_t pin, uint8_t pinType ) :
    BaseModule( eepromHelper, eepromIndex, pin, pinType )
{
}

/**
 * Initialize 3 steps:
 * - Pins
 * - The four variables
 * - Initial values
 */
void BrightnessModule::initialize()
{
    // Tell Eeprom memory usage
    //_eepromHelper->moduleMemoryUsage( _eepromIndex, 50 );
    
    // Pins
    pinMode( _pin, INPUT );
    
    // The four variables
    _readable       = true;
    _writable       = false;
    _eepromSaveable = false;
    strncpy( _moduleType, MODULE_BRIGHTNESS, 4 );
    
    // Done
    _initialized = true;
    
    // Values (requires _initialized for read() method)
    for( uint8_t i=0; i<BRIGHTNESS_BUFFER_SIZE; i++ )
        _sensorBuffer[i] = read();
    _currentValue = _sensorBuffer[BRIGHTNESS_BUFFER_SIZE-1];
    
#ifdef MODULE_BRIGHTNESS_DEBUG
        Serial.println("Brightness module initialized");
#endif
}

/**
 * Reads the brightness sensor module status in json format to the buffer
 *
 * Format:
 *  {"id":1,"type":"bri","live":100,"average":100}
 */
uint8_t BrightnessModule::readStatus( char buffer[MODULE_BUFFER_SIZE] )
{
    if( !_initialized )
        return false;
    
    memset( &buffer[0], 0, MODULE_BUFFER_SIZE );

    uint8_t bufferPtr = 0;
    char number[5];
    
    // Add id: '{"id":1' (note no comma in end)
    bufferPtr = addModuleJsonIdToBuffer( buffer );
    
    // Add type
    strncpy( &buffer[bufferPtr], ",\"type\":\"", 9 );
    bufferPtr += 9;
    strncpy( &buffer[bufferPtr], _moduleType, 3 );
    bufferPtr += 3;
        
    // Add live
    strncpy( &buffer[bufferPtr], "\",\"live\":", 9 );
    bufferPtr += 9;
        
    itoa( _latestLiveValuePercentage, number, 10 );
    for( uint8_t i=0; i< strlen( number ); i++ )
        buffer[bufferPtr++] = number[i];
        
    // Add average
    strncpy( &buffer[bufferPtr], ",\"average\":", 11 );
    bufferPtr += 11;
    
    itoa( _latestAverageValuePercentage, number, 10 );
    for( uint8_t i=0; i< strlen( number ); i++ )
        buffer[bufferPtr++] = number[i];
        
    buffer[bufferPtr++] = '}';

#ifdef MODULE_BRIGHTNESS_DEBUG
        Serial.print("BrightnessModule: latest status added bytes "); 
        Serial.println(bufferPtr);
#endif
    
    return bufferPtr;
}

void BrightnessModule::handleSecondPoll()
{
    if( !_initialized )
        return;
    
    _sensorBuffer[_sensorBufferIndex] = read();
    _currentValue = _sensorBuffer[_sensorBufferIndex];
    _sensorBufferIndex++;
    
    if( _sensorBufferIndex >= BRIGHTNESS_BUFFER_SIZE )
        _sensorBufferIndex = 0;
    
    uint16_t sensorAvg = 0;
    for( uint8_t i=0; i<BRIGHTNESS_BUFFER_SIZE; i++ )
        sensorAvg += _sensorBuffer[i];
    sensorAvg = sensorAvg >> BRIGHTNESS_BUFFER_DIVIDER;
    
    // Invert values ( 0 is highest, 1023 is lowest => 1024 is highest, 1 is lowest )
    uint32_t current = -_currentValue + 1024;
    uint32_t average = -sensorAvg + 1024;
    
    // To percentages
    _latestLiveValuePercentage      = (current*100) >> 10; // Divide by 1024
    _latestAverageValuePercentage   = (average*100) >> 10; // Divide by 1024
}


/************************************************************************/
/*                          RESTFUL METHODS                             */
/************************************************************************/

/**
 * Read brightness and average brightness
 */
uint8_t BrightnessModule::restfulRead( char buffer[MODULE_BUFFER_SIZE] )
{
    if( !_initialized )
        return false;
    
    memset( &buffer[0], 0, MODULE_BUFFER_SIZE );
    
    if( _jsonChuckCounter == 0 ) {
        uint8_t bufferPtr = 0;
        char number[5];
        
        // Add type
        strncpy( &buffer[bufferPtr], ",\"type\":\"", 9 );
        bufferPtr += 9;
        strncpy( &buffer[bufferPtr], _moduleType, 3 );
        bufferPtr += 3;
            
        // Add the latest value as 'live'
        strncpy( &buffer[bufferPtr], "\",\"value\":", 10 );
        bufferPtr += 10;
        
        itoa( _latestLiveValuePercentage, number, 10 );
        for( uint8_t i=0; i<strlen( number ); i++ )
            buffer[bufferPtr++] = number[i];
        
        // Add the average value for latest SENSOR_BUFFER_SIZE seconds
        strncpy( &buffer[bufferPtr], ",\"average\":", 11 );
        bufferPtr += 11;
        
        itoa( _latestAverageValuePercentage, number, 10 );
        for( uint8_t i=0; i<strlen( number ); i++ )
            buffer[bufferPtr++] = number[i];
        
        _jsonChuckCounter++;

#ifdef MODULE_BRIGHTNESS_DEBUG
        Serial.print("BrightnessModule: restful read added bytes ");
        Serial.println(bufferPtr);
#endif
        
        return true;
    }
    
    _jsonChuckCounter = 0;
    return false;
}
