
#ifndef _BRIGTHNESSMODULE_H_
#define _BRIGTHNESSMODULE_H_

#include "BaseModule.h"

//#define MODULE_BRIGHTNESS_DEBUG 1

#define BRIGHTNESS_BUFFER_SIZE      32  
#define BRIGHTNESS_BUFFER_DIVIDER   5   // Determines how many times buffer sum is shifted (divided), update if bufferSize changes

class BrightnessModule : public BaseModule 
{
public:
    BrightnessModule( EepromHelper* eepromHelper, uint8_t eepromIndex, uint8_t pin, uint8_t pinType = 'a' );
    void initialize();
    uint8_t readStatus( char buffer[MODULE_BUFFER_SIZE] );
    
    void handleSecondPoll();
    
    // Overrided CRUD methods
    uint8_t restfulRead( char buffer[MODULE_BUFFER_SIZE] );     // GET
    
private:
    uint8_t _sensorBufferIndex;
    uint16_t _sensorBuffer[BRIGHTNESS_BUFFER_SIZE];
    
    uint8_t _latestLiveValuePercentage;
    uint8_t _latestAverageValuePercentage;
};

#endif
