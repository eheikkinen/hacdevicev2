#ifndef _DEVICE_H_
#define _DEVICE_H_

#include "Arduino.h"
#include <ArduinoJson.h>

#include "HAL.h"
#include "EepromHelper.h"

//#define DEVICE_DEBUG 1

class Device
{
public:
    Device( EepromHelper* eepromHelper, uint8_t ledPin );
    
    void setDeviceNameFromMacAddress( uint8_t macAddress[6] );
    char* getDeviceNamePtr();
    char* getHumanDeviceNamePtr();
    void setHumanDeviceName( char* humanDeviceName, uint8_t length );
    uint8_t getRssi();
    void setRssi( uint8_t rssi );
    uint8_t isDeviceNameSet();
    void initiateBlinkRate();
    uint8_t getBlinkRate();
    void setBlinkRate( uint8_t rate );
    void handleLed( uint16_t counter );
    void turnOffLed();
    
    uint8_t restfulUpdate( char buffer[MODULE_BUFFER_SIZE] ); // PUT
    
private:
    char    _deviceNameSet;
    char    _deviceName[DEVICE_NAME_LENGTH];
    char    _humanDeviceName[HUMAN_NAME_LENGTH];
    uint8_t _rssi;
    uint8_t _macAddress[6];
    
    uint8_t _ledPin;
    uint8_t _ledValue;
    uint8_t _keepLedOffCounter;
    volatile uint8_t _blinkRate;
    
    EepromHelper* _eepromHelper;
};

#endif
