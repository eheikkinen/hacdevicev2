
#ifndef _EEPROMHELPER_H_
#define _EEPROMHELPER_H_

#include <EEPROM.h>
#include "HAL.h"

#define EEPROM_HELPER_DEBUG         1

// Eeprom helper uses this to verify how to use the eeprom memory
#define EEPROM_VERSION              1

#define EEPROM_VERSION_ADDRESS      0
#define EEPROM_DEVICE_NAME_ADDRESS  1
#define EEPROM_HUMAN_NAME_ADDRESS   EEPROM_DEVICE_NAME_ADDRESS + DEVICE_NAME_LENGTH
#define EEPROM_MODULES_ADDRESS      EEPROM_HUMAN_NAME_ADDRESS + HUMAN_NAME_LENGTH

typedef struct {
    uint8_t addressSet;
    uint8_t startAddress;
    uint8_t endAddress;
} ModuleMemoryUsage;


/**
 * Class EepromHelper WIP
 *
 * TODO move source to cpp!
 */
class EepromHelper
{
public:

    EepromHelper( EEPROMClass eeprom )
    {
        _eeprom = eeprom;
        for( uint8_t i=0; i<MODULE_COUNT; i++ ) {
            _moduleMemoryAddresses[i].addressSet    = 0;
            _moduleMemoryAddresses[i].startAddress  = 0;
            _moduleMemoryAddresses[i].endAddress    = 0;
        }
        
        //clearEepromMemory( -1 );
    }
    
    uint8_t moduleMemoryUsage( uint8_t moduleIndex, uint8_t memoryUsage )
    {
        Serial.print("Setting module ");
        Serial.print(moduleIndex+1); Serial.print(" memory usage to "); Serial.println(memoryUsage);
        
        if( _moduleMemoryAddresses[moduleIndex].addressSet )
            return false;
        
        uint16_t startAddress = EEPROM_MODULES_ADDRESS;
        
        // Find modules start address
        for( uint8_t i=0; i<moduleIndex; i++ ) {
            if( _moduleMemoryAddresses[i].addressSet )
                startAddress = _moduleMemoryAddresses[i].endAddress;
        }
        
        // Save it
        _moduleMemoryAddresses[moduleIndex].startAddress = startAddress;
        _moduleMemoryAddresses[moduleIndex].endAddress = startAddress + memoryUsage;
        _moduleMemoryAddresses[moduleIndex].addressSet = true;
        
        Serial.print("-- Module eeprom address range: ");
        Serial.print(_moduleMemoryAddresses[moduleIndex].startAddress);
        Serial.print("-");
        Serial.println(_moduleMemoryAddresses[moduleIndex].endAddress);
        
        // Check if we need to move modules after this forward
        for( uint8_t i=moduleIndex+1; i<MODULE_COUNT; i++ ) {
            if( _moduleMemoryAddresses[i].addressSet ) {
                _moduleMemoryAddresses[i].startAddress  += memoryUsage;
                _moduleMemoryAddresses[i].endAddress    += memoryUsage;
            }
        }
    }
    
    void checkEepromVersion()
    {
        uint8_t version = getEepromMemoryVersion();
        
        if( version != false && version != EEPROM_VERSION ) {
            // Data stored in the eeprom is in different order!
            clearEepromMemory( -1 );
            saveEepromMemoryVersion( EEPROM_VERSION );
        }
        else if( !version ) {
            // Version has not been saved yet, first processor start
            saveEepromMemoryVersion( EEPROM_VERSION );
        }
        else {
#ifdef EEPROM_HELPER_DEBUG
            Serial.println(F("EEPROM: Memory version is correct"));
#endif
        }
    }
    
    uint8_t getDeviceNames( char deviceName[DEVICE_NAME_LENGTH], char humanDeviceName[HUMAN_NAME_LENGTH] )
    {
        // Try to get human name
        getEepromHumanDeviceName( humanDeviceName );
        
        // Return true if name was found, else returns false
        return getEepromDeviceName( deviceName );
    }
    
    void saveEepromDeviceName( char deviceName[DEVICE_NAME_LENGTH] )
    {
        for( uint8_t i=0; i<DEVICE_NAME_LENGTH; i++ )
            _eeprom.write( EEPROM_DEVICE_NAME_ADDRESS+i, deviceName[i] );
    }
    
    void saveEepromHumanDeviceName( char humanDeviceName[HUMAN_NAME_LENGTH] )
    {
        for( uint8_t i=0; i<HUMAN_NAME_LENGTH; i++ ) {
            // For some reason single liner doesn't work here!
            uint16_t address = EEPROM_HUMAN_NAME_ADDRESS+i;
            _eeprom.write( address, humanDeviceName[i] );
        }
    }

    uint8_t getEepromModuleData( uint8_t moduleIndex, char buffer[] )
    {
        if( moduleIndex >= MODULE_COUNT || !_moduleMemoryAddresses[moduleIndex].addressSet )
            return false;
        Serial.println("EepromHelper: Getting module data!");   
        uint8_t dataAddress = 0;
        for( uint8_t i=_moduleMemoryAddresses[moduleIndex].startAddress; i<_moduleMemoryAddresses[moduleIndex].endAddress; i++ )
            buffer[dataAddress++] = _eeprom.read( i );
        
        dumpEeprom();
        
        return true;
    }
    
    uint8_t saveEepromModuleData( uint8_t moduleIndex, char buffer[] )
    {
        if( moduleIndex >= MODULE_COUNT || !_moduleMemoryAddresses[moduleIndex].addressSet )
            return false;
            
        Serial.print("EepromHelper: Saving module data to eeprom address range: ");
        Serial.print(_moduleMemoryAddresses[moduleIndex].startAddress);
        Serial.print("-");
        Serial.println(_moduleMemoryAddresses[moduleIndex].endAddress);
        
        uint8_t dataAddress = 0;
        for( uint8_t i=_moduleMemoryAddresses[moduleIndex].startAddress; i<_moduleMemoryAddresses[moduleIndex].endAddress; i++ )
            _eeprom.write( i, buffer[dataAddress++] );
        
        dumpEeprom();
        
        return true;
    }
    
    void dumpEeprom()
    {
        Serial.println(" --- EEPROM DUMP START --- ");
        Serial.print("Version: "); Serial.println( _eeprom.read( 0 ), DEC);
        
        Serial.print("Name: "); 
        for( uint16_t i=EEPROM_DEVICE_NAME_ADDRESS; i<EEPROM_DEVICE_NAME_ADDRESS+DEVICE_NAME_LENGTH; i++ )
            Serial.write( _eeprom.read(i) );
        Serial.println("");
        
        Serial.print("Human Name: ");
        for( uint16_t i=EEPROM_HUMAN_NAME_ADDRESS; i<EEPROM_HUMAN_NAME_ADDRESS+HUMAN_NAME_LENGTH; i++ )
            Serial.write( _eeprom.read(i) );
        Serial.println("");
        
        for( uint8_t i=0; i<MODULE_COUNT; i++ ) {
            Serial.print("-- Module: ");
            Serial.println(i);
            
            if( !_moduleMemoryAddresses[i].addressSet ) {
                Serial.println("-- Module has no eeprom memory range set");
                continue;
            }
            
            Serial.print("-- Module memory range: ");
            Serial.print(_moduleMemoryAddresses[i].startAddress);
            Serial.print("-");
            Serial.println(_moduleMemoryAddresses[i].endAddress);
            
            
            for( uint8_t j=_moduleMemoryAddresses[i].startAddress; j<_moduleMemoryAddresses[i].endAddress; j++ ) {
                Serial.print( _eeprom.read( j ) );
                Serial.print("-");
            }
            
            Serial.println("");
        }
        
        Serial.println(" --- EEPROM DUMP END --- ");
    }


    void clearEepromMemory( int8_t moduleIndex )
    {
        if( moduleIndex == -1 ) {
            for( uint8_t i=0; i<250; i++ )
                _eeprom.write( i, 0xFF );
        }
        else {
#ifdef EEPROM_HELPER_DEBUG
            Serial.print(F("EEPROM: Clearing used EEPROM memory from module index "));
            Serial.println(moduleIndex);
#endif
            
            if( moduleIndex >= MODULE_COUNT )
                return;
            
            for( uint8_t i=_moduleMemoryAddresses[moduleIndex].startAddress; i<_moduleMemoryAddresses[moduleIndex].endAddress; i++ )
                _eeprom.write( i, 0xFF );
        }
    }

private:
    
    uint8_t getEepromMemoryVersion()
    {
        // Stored in the first byte
        uint8_t version = _eeprom.read( EEPROM_VERSION_ADDRESS );
        
        // Not set
        if( version == 0xFF ) {
#ifdef EEPROM_HELPER_DEBUG
            Serial.println(F("EEPROM: EEPROM version not set"));
#endif

            return false;
        }

#ifdef EEPROM_HELPER_DEBUG
        Serial.print(F("EEPROM: Eeprom version set: "));
        Serial.println(version);
#endif
        
        return version;
    }

    void saveEepromMemoryVersion( uint8_t memoryVersion )
    {
#ifdef EEPROM_HELPER_DEBUG
        Serial.print(F("EEPROM: Saving memory version to EEPROM: "));
        Serial.println(memoryVersion);
#endif
        _eeprom.write( EEPROM_VERSION_ADDRESS, memoryVersion );
    }

    uint8_t getEepromDeviceName( char deviceName[DEVICE_NAME_LENGTH] )
    {
        for( uint8_t i=0; i<DEVICE_NAME_LENGTH; i++ ) {
            uint8_t c = _eeprom.read( EEPROM_DEVICE_NAME_ADDRESS+i );
            
            if( c == 0xFF ) {
#ifdef EEPROM_HELPER_DEBUG
                Serial.print(F("EEPROM: Name doesnt exist in EEPROM. Empty index "));
                Serial.println(i);
#endif

                return false; // Return false, before we sign anything to device name if name not set
            }
            
            deviceName[i] = c;
        }

#ifdef EEPROM_HELPER_DEBUG
        Serial.print(F("EEPROM: Device name found: "));
        Serial.println(deviceName);
#endif
//return false; // REMOVE ME!
        return true;
    }
    
    void getEepromHumanDeviceName( char humanDeviceName[HUMAN_NAME_LENGTH] )
    {
        uint16_t startAddress = EEPROM_HUMAN_NAME_ADDRESS;
        for( uint8_t i=0; i<HUMAN_NAME_LENGTH; i++ ) {
            char c = _eeprom.read( startAddress++ );
            if( i == 0 && c == -1 ) {
                memset( humanDeviceName, 0, HUMAN_NAME_LENGTH );
                return;
            }
            
            humanDeviceName[i] = c;
        }
    }
    
    void dumpMemorySets()
    {
        Serial.println( "EepromHelper: Memory addresses for modules:");
        for( uint8_t i=0; i<MODULE_COUNT; i++ ) {
            Serial.print(i);
            Serial.print(" setted: ");
            Serial.print( _moduleMemoryAddresses[i].addressSet );
            Serial.print(" : ");
            Serial.print( _moduleMemoryAddresses[i].startAddress );
            Serial.print("-");
            Serial.println( _moduleMemoryAddresses[i].endAddress );
        }
    }
    
    // Variables
    EEPROMClass _eeprom;
    ModuleMemoryUsage _moduleMemoryAddresses[MODULE_COUNT];
};
#endif
