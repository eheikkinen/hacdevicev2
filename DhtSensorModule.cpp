#include "DhtSensorModule.h"

DhtSensorModule::DhtSensorModule( EepromHelper* eepromHelper, uint8_t eepromIndex,uint8_t pin, uint8_t pinType ) :
    BaseModule( eepromHelper, eepromIndex, pin, pinType )
{
}

/**
 * Initialize 3 steps:
 * - Pins
 * - The four variables
 * - Initial values
 */
void DhtSensorModule::initialize()
{
    // Pins
    pinMode( _pin, INPUT );
    
    // The four variables
    _readable       = true;
    _writable       = false;
    _eepromSaveable = false;
    strncpy( _moduleType, MODULE_SENSOR_DHT, 4 );
    
    // Values
    _temperature    = 0;
    _humidity       = 0;
    
    // Done
    _initialized    = true;
    
#ifdef MODULE_DHTSENSOR_DEBUG
    Serial.println(F("Dht module initialized"));
#endif
}

/**
 * Reads the DHT sensor module status in json format to the buffer
 *
 * Format:
 *  {"id":1,"type":"tem","temperature":250,"humidity":100}
 */
uint8_t DhtSensorModule::readStatus( char buffer[MODULE_BUFFER_SIZE] )
{
    if( !_initialized )
        return false;
    
    memset( &buffer[0], 0, MODULE_BUFFER_SIZE );
    
    // Requires query, read until success ( SLOW :( )
    while( readDht() );

    uint8_t bufferPtr = 0;
    char number[5];
    
    // Add id: '{"id":1' (note no comma in end)
    bufferPtr = addModuleJsonIdToBuffer( buffer );
    
    // Add type
    strncpy( &buffer[bufferPtr], ",\"type\":\"", 9 );
    bufferPtr += 9;
    strncpy( &buffer[bufferPtr], _moduleType, 3 );
    bufferPtr += 3;
    
    // Add temperature
    strncpy( &buffer[bufferPtr], "\",\"temperature\":", 16 );
    bufferPtr += 16;
        
    itoa( _temperature, number, 10 );
    for( uint8_t i=0; i< strlen( number ); i++ )
        buffer[bufferPtr++] = number[i];
    
    // Add humidity
    strncpy( &buffer[bufferPtr], ",\"humidity\":", 12 );
    bufferPtr += 12;
        
    itoa( _humidity, number, 10 );
    for( uint8_t i=0; i< strlen( number ); i++ )
        buffer[bufferPtr++] = number[i];
    
    buffer[bufferPtr++] = '}';

#ifdef MODULE_DHTSENSOR_DEBUG
    Serial.print("DhtSensorModule: latest status added bytes ");
    Serial.println(bufferPtr);
#endif
    
    return bufferPtr;
}

uint8_t DhtSensorModule::readDht()
{
    uint8_t bits[5] = {0,0,0,0,0};
    uint8_t cnt = 7;
    uint8_t idx = 0;

    // Request sample
    pinMode( _pin, OUTPUT );
    digitalWrite( _pin, LOW );
    delay( 18 );
    digitalWrite( _pin, HIGH );
    delayMicroseconds( 40 );
    pinMode( _pin, INPUT );

    // Acknowledge or timeout
    unsigned int loopCnt = 10000;
    while( digitalRead( _pin ) == LOW )
        if( loopCnt-- == 0 ) return false;

    loopCnt = 10000;
    while( digitalRead( _pin ) == HIGH )
        if( loopCnt-- == 0 ) return false;

    // Read the output - 5 bytes or timeout
    for (int i=0; i<40; i++) {
        loopCnt = 10000;
        while( digitalRead( _pin ) == LOW )
            if( loopCnt-- == 0 ) return false;

        unsigned long t = micros();

        loopCnt = 10000;
        while( digitalRead( _pin ) == HIGH )
            if( loopCnt-- == 0 ) return false;

        if( ( micros() - t ) > 40 ) 
            bits[idx] |= (1 << cnt);
        
        if( cnt == 0 ) {
            cnt = 7;    // restart at MSB
            idx++;      // next byte!
        }
        else
            cnt--;
    }
    
    // Check checksum
    if( bits[4] != ( bits[0] + bits[2] ) ) {
#ifdef MODULE_DHTSENSOR_DEBUG
        Serial.println("DhtSensorModule: Invalid checksum from DHT");
#endif
        // Todo add retry read logic
        return false;
    }
    
    _temperature    = bits[2];
    _humidity       = bits[0];
    
    return true;
}


/************************************************************************/
/*                          RESTFUL METHODS                             */
/************************************************************************/

/**
 * Read temperature (C) and humidity (%)
 */
uint8_t DhtSensorModule::restfulRead( char buffer[MODULE_BUFFER_SIZE] )
{
    if( !_initialized )
        return false;
    
    memset( &buffer[0], 0, MODULE_BUFFER_SIZE );
    
    // Read until success
    while( readDht() );
    
    if( _jsonChuckCounter == 0 ) {
        uint8_t bufferPtr = 0;
        char number[5];
        
        // Add type
        strncpy( &buffer[bufferPtr], ",\"type\":\"", 9 );
        bufferPtr += 9;
        strncpy( &buffer[bufferPtr], _moduleType, 3 );
        bufferPtr += 3;
            
        // Add the temperature
        strncpy( &buffer[bufferPtr], "\",\"temperature\":", 16 );
        bufferPtr += 16;
        
        itoa( _temperature, number, 10 );
        for( uint8_t i=0; i<strlen( number ); i++ )
            buffer[bufferPtr++] = number[i];
        
        // Add the humidity
        strncpy( &buffer[bufferPtr], ",\"humidity\":", 12 );
        bufferPtr += 12;
        
        itoa( _humidity, number, 10 );
        for( uint8_t i=0; i<strlen( number ); i++ )
            buffer[bufferPtr++] = number[i];
        
        _jsonChuckCounter++;

#ifdef MODULE_DHTSENSOR_DEBUG
        Serial.print("DhtSensorModule: restful read added bytes ");
        Serial.println(bufferPtr);
#endif
        
        return true;
    }
    
    _jsonChuckCounter = 0;
    return false;
}
