#ifndef _TIMERMODULE_H_
#define _TIMERMODULE_H_

#include "BaseModule.h"
#include <RTClib.h>
#include <ArduinoJson.h>

//#define MODULE_TIMER_DEBUG

#define TIMER_WILDCARD          '@' // Has to be higher than 59 binary (seconds)
#define TIMER_EEPROM_SIZE       50
#define TIMER_EEPROM_SINGLE_ROW	5
#define TIMER_ROW_COUNT         TIMER_EEPROM_SIZE / TIMER_EEPROM_SINGLE_ROW // Currently supports 10 timers! If you increase, the ModuleRestApi2 buffer size needs to be increased aswell

class DateTime;

class TimerModule : public BaseModule 
{
public:
    TimerModule( EepromHelper* eepromHelper, uint8_t eepromIndex, uint8_t pin, uint8_t pinType = 'd' );
    void initialize();
    uint8_t readStatus( char buffer[MODULE_BUFFER_SIZE] );

    void handleSecondPoll( DateTime* now );

    void getTimeString( uint8_t time[] );

    // Overrided CRUD methods
    uint8_t restfulCreate( char buffer[MODULE_BUFFER_SIZE] );   // POST
    uint8_t restfulRead( char buffer[MODULE_BUFFER_SIZE] );     // GET
    uint8_t restfulUpdate( char buffer[MODULE_BUFFER_SIZE] );   // PUT
    uint8_t restfulDelete( char buffer[MODULE_BUFFER_SIZE] );   // DELETE

private:
    int16_t compare( DateTime* dt );

    // Handle eeprom data
    void initializeTimersData();
    uint8_t saveTimerRow( uint8_t hh, uint8_t mm, uint8_t ss, int16_t value, int8_t index = -1);
    uint8_t deleteTimerRow( uint8_t index );

    int8_t validateHours( const char* hh );
    int8_t validateMinutesAndSeconds( const char* ss );
    int8_t validateId( const char* id );

#ifdef MODULE_TIMER_DEBUG
    void dumpCurrentTimerDatas();
#endif

    char    _timersData[TIMER_EEPROM_SIZE];
    uint8_t _setTimerCount;
    uint8_t _timerIndexSet[TIMER_ROW_COUNT];

    uint8_t _hh;
    uint8_t _mm;
    uint8_t _ss;
};

#endif
