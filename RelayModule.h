#ifndef _RELAYMODULE_H_
#define _RELAYMODULE_H_

#include "BaseModule.h"
#include <ArduinoJson.h>

//#define MODULE_RELAY_DEBUG 1

class RelayModule : public BaseModule 
{
public:
    RelayModule( EepromHelper* eepromHelper, uint8_t eepromIndex, uint8_t pin, uint8_t pinType = 'd' );

    void initialize();
    uint8_t readStatus( char buffer[MODULE_BUFFER_SIZE] );
    
    // Overrided CRUD methods
    uint8_t restfulRead( char buffer[MODULE_BUFFER_SIZE] );     // GET
    uint8_t restfulUpdate( char buffer[MODULE_BUFFER_SIZE] );   // PUT
    
private:

};

#endif
