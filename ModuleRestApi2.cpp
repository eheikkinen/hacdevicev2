
#include "ModuleRestApi2.h"

// Public methods ////////////////////////////////////////////////////

ModuleRestApi2::ModuleRestApi2( Device* device )
{
    _device = device;
    
    memset( &_answer[0], 0, sizeof(_answer) );
    _answerIndex            = 0;
    _method                 = 0;
    _command                = 0;
    _moduleIndex            = 255;
    _moduleValue            = 9999;
    _packetReceived         = false;
    _moduleSelected         = false;
    _outputBufferIndex      = 0;
    _modulesAdded           = 0;
    _urlEndVerified         = false;
    _answerIsError          = false;
}
    
/**
 * Handles a http request if data has been received
 *
 * @param Adafruit_CC3000_ClientRef &client
 * @return uint8_t (0/1)
 */
uint8_t ModuleRestApi2::handle( Adafruit_CC3000_ClientRef &client )
{
    if( client.available() ) {
        if( !processReceivedData( client ) ) {
#ifdef MODULE_REST_API_DEBUG
            Serial.println("Received invalid headers");
#endif
            buildBadRequestAnswer();
        }
        else {
            _device->turnOffLed();
            _packetReceived = true;

#ifdef MODULE_REST_API_DEBUG
            Serial.write( _method );
            Serial.println();
            Serial.write( _command );
            Serial.println();
            Serial.println( _moduleIndex );
            Serial.println( _moduleValue );
#endif
            
            uint8_t successOrId = validateAndExecuteReceivedData();
            
            if( !successOrId )
                buildBadRequestAnswer();
            else
                buildSuccessAnswer( successOrId );

#ifdef MODULE_REST_API_DEBUG
            Serial.print("ModuleRestApi: Sending payload: ");
            Serial.println( &_outputBuffer[163] ); // Hide headers
#endif
        }
            
        sendResponse( client, 32, 100 );
            
        client.stop();
            
        resetStatus();
            
        return true;
    }
    return false;
}

void ModuleRestApi2::addModule( BaseModule* module )
{
    if( _modulesAdded >= MODULE_COUNT )
        Serial.println("ModuleRestApi: Unable to add more than MODULE_COUNT modules");
    
    _modules[_modulesAdded++] = module;
}

void ModuleRestApi2::addModules( BaseModule* modules[MODULE_COUNT] )
{
    for( uint8_t i=0; i<MODULE_COUNT; i++ )
        _modules[i] = modules[i];
    _modulesAdded = MODULE_COUNT;
}

uint8_t ModuleRestApi2::getPacketReceived()
{
    uint8_t tmp = _packetReceived;
    _packetReceived = false;
    return tmp;
}

// Private methods ////////////////////////////////////////////////////

uint8_t ModuleRestApi2::processReceivedData( Adafruit_CC3000_ClientRef &client )
{
#ifdef MODULE_REST_API_DEBUG
    Serial.println("---------------");
#endif
        
    while( client.available() ) {
        char c = client.read();
        
        if( _answerIndex >= MODULERESTAPI2_INPUT_BUFFER_SIZE )
            continue;
        
        _answer[_answerIndex++] = c;
        
        if( !processAcquiredData( c ) ) {
            // Received invalid headers, empty the buffer and ignore it
            while( client.available() )
                c = client.read();
            
            return false;
        }
    }
    return true;
}

/**
 * Checks the rows received by HTTP and adjusts
 * _method, _command and _moduleSelected
 *
 * First stage:     find the HTTP method
 * Second stage:    find supported command
 * Third stage:     find command module
 * Fourth stage:    find url end
 *
 * @param char c
 */
uint8_t ModuleRestApi2::processAcquiredData( char c )
{
    if( ( c == '/' || c == '\r' ) ) {
        // Fourth stage (check the end)
        if( _command == 'm' && _moduleSelected && !_urlEndVerified ) {
            // Check if read/write
            if( ( _answer[0] != '1' && _answer[1] != '.' && _answer[2] != '1' ) )
                return false;

            _urlEndVerified = true;
        }
        
        // Third stage (check for the module)
        if( _command == 'm' && !_moduleSelected ) {
            // Module between 1-9
            if( _answer[0] < '1' || _answer[0] > '9' )
                return false;
                
            if( _answer[1] != ' ' && ( _answer[1] != '/' && strlen( _answer ) == 2 ) )
                return false;
            
            _moduleIndex = _answer[0] - 49; // Ascii -> int and number(1) to index(0)

            _moduleSelected = true;
        }
            
        // Second stage (check for the command) - Todo use defines
        if( _method != 0 && strncmp( _answer, "module/", 7 ) == 0 ) {
            _command = 'm';
        }
        else if( _method != 0 && strncmp( _answer, "device ", 6 ) == 0 ) {
            _command = 'd';
        }
        
        // First stage (check the HTTP method) - Todo use defines
        if( _method == 0 ) {
            if( strcmp( _answer, "GET /" ) == 0 )
                _method = 'g';
            else if ( strcmp( _answer, "POST /" ) == 0 )
                _method = 'p';
            else if ( strcmp( _answer, "DELETE /" ) == 0 )
                _method = 'd';
            else if ( strcmp( _answer, "PUT /" ) == 0 )
                _method = 'u';
            else if ( strcmp( _answer, "OPTIONS /" ) == 0 )
                _method = 'o';
        }
        
        memset( &_answer[0], 0, sizeof(_answer) );
        _answerIndex = 0;
    }
    
    return true;
}
    
/**
 * Validates and executes the received data
 * 
 * @return uint8_t (0/1)
 */
uint8_t ModuleRestApi2::validateAndExecuteReceivedData( void )
{
    switch( _method )
    {
        case 'g': // GET
            if( _command == 'm' ) { // Module specific read supported
                // Validate module number
                if( _moduleIndex > ( MODULE_COUNT - 1 ) )
                    return false;
                
                // Returns -1 on failure
                if( _modules[_moduleIndex]->read() != -1 )
                    return true;
            }
            else if( _command == 'd' ) { // General device supported
                return true;
            }
            break;
            
        case 'p': // POST
            if( _command == 'm' ) { // Module specific write (new) supported
                // Validate module number
                if( _moduleIndex > ( MODULE_COUNT - 1 ) )
                    return false;

#ifdef MODULE_REST_API_DEBUG
                Serial.print("Adding new with json:"); Serial.println(_answer);
#endif
                
                uint8_t id = _modules[_moduleIndex]->restfulCreate( _answer );
                
                if( !id ) {
                    _answerIsError = true; // If query failed, _answer has error msg
                    return false;
                }
                
                return id;
            }
            break;
        
        case 'u': // PUT
            if( _command == 'm' ) { // Module specific write (update) supported
                // Validate module number
                if( _moduleIndex > ( MODULE_COUNT - 1 ) )
                    return false;

#ifdef MODULE_REST_API_DEBUG
                Serial.print("Updateing existing with json:"); Serial.println(_answer);
#endif
                
                if( !_modules[_moduleIndex]->restfulUpdate( _answer ) ) {
                    _answerIsError = true; // If query failed, _answer has error msg
                    return false;
                }

                return true;
            }
            else if( _command == 'd' ) { // Device specific write (update) supported
                if( !_device->restfulUpdate( _answer ) ) {
                    _answerIsError = true; // Todo improve this variable logic
                    return false;
                }

                return true;
            }
            break;
        
        case 'd': // DELETE
            if( _command == 'm' ) { // Module specific write (update) supported
                // Validate module number
                if( _moduleIndex > ( MODULE_COUNT - 1 ) )
                    return false;

#ifdef MODULE_REST_API_DEBUG
                Serial.print("Deleting existing with json:"); Serial.println(_answer);
#endif
                
                if( !_modules[_moduleIndex]->restfulDelete( _answer ) ) {
                    _answerIsError = true; // If query failed, _answer has error msg
                    return false;
                }
                return true;
            }
            break;
            
        case 'o': // OPTIONS (CORS preflight)
            return true;
            break;
    }
    
    return false;
}
    
void ModuleRestApi2::buildBadRequestAnswer( void )
{
#ifdef MODULE_REST_API_DEBUG
    Serial.println("----- Building bad request answer");
#endif
        
    // Status code 400
    addToBuffer(F("HTTP/1.1 400 Bad Request\r\nAccess-Control-Allow-Origin: *\r\nAccess-Control-Allow-Methods: POST, GET, PUT, OPTIONS\r\nContent-Type: application/json\r\nConnection: close\r\n\r\n"));
    
    if( _answerIsError ) {
        addToBuffer("{\"status\":0,\"error\":\"");
        addToBuffer(_answer);
        addToBuffer("\"}");
    }
    else {
        addToBuffer(F("{\"status\":0}"));
    }
}


void ModuleRestApi2::buildSuccessAnswer( uint8_t id )
{
#ifdef MODULE_REST_API_DEBUG
    Serial.println("----- Building success answer");
#endif
        
    // Status code 200
    addToBuffer(F("HTTP/1.1 200 OK\r\nAccess-Control-Allow-Origin: *\r\nAccess-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS\r\nContent-Type: application/json\r\nConnection: close\r\n\r\n")); // 177 bytes!
    addToBuffer(F("{\"status\":1"));
    
    char moduleJsonBuffer[MODULE_BUFFER_SIZE];
    
    switch( _method )
    {
        case 'g': // GET
            switch( _command ) {
                case 'm': // MODULE
                    while( _modules[_moduleIndex]->restfulRead( moduleJsonBuffer ) )
                        addToBuffer( (char*)moduleJsonBuffer );
                
                    break;
                
                case 'd': // DEVICE
                    addToBuffer( F(",\"name\":\""));
                    addToBuffer( _device->getDeviceNamePtr() );
                    
                    addToBuffer( F("\",\"human_name\":\""));
                    addToBuffer( _device->getHumanDeviceNamePtr() );
                    
                    addToBuffer( F("\",\"signal\":"));
                    addToBuffer( _device->getRssi() );
                    
                    addToBuffer( F(",\"modules\":[") );
                    for( uint8_t i=0; i<MODULE_COUNT; i++ ) {
                        if( i != 0 )
                            addToBuffer( "," );
                        
                        _modules[i]->readStatus( moduleJsonBuffer );
                        addToBuffer( (char*)moduleJsonBuffer );
                    }
                    addToBuffer( F("]") );
                    
                    break;
            }
            break;
            
        case 'p': // POST
            addToBuffer(F(",\"id\":"));
            addToBuffer( id );
            addToBuffer(F(",\"msg\":\"Adding success\""));
            break;
            
        case 'u': // PUT
            addToBuffer(F(",\"msg\":\"Update success\""));
            break;
            
        case 'd': // DELETE
            addToBuffer(F(",\"msg\":\"Delete success\""));
            break;
    }
    
    addToBuffer(F("}"));
}
    
/**
 * Sends the output buffer in chunks to the CC3000
 */
void ModuleRestApi2::sendResponse( Adafruit_CC3000_ClientRef &client, uint8_t chunkSize, uint8_t wait )
{
    // Max iteration
    uint8_t max_iteration = (int)( _outputBufferIndex/chunkSize ) + 1;
        
    // Send data
    for (uint8_t i = 0; i < max_iteration; i++ ) {
        char intermediate_buffer[chunkSize+1];
        memcpy( intermediate_buffer, _outputBuffer + i*chunkSize, chunkSize );
        intermediate_buffer[chunkSize] = '\0';
            
        // Send intermediate buffer
        #ifdef ADAFRUIT_CC3000_H
        client.fastrprint( intermediate_buffer );
        #else
        client.print( intermediate_buffer );
        #endif
    }
        
    // Wait for the client to get data
    delay( wait );
    memset( &_outputBuffer[0], 0, sizeof(_outputBuffer) );
}
    
void ModuleRestApi2::resetStatus( void )
{
    memset( &_answer[0], 0, sizeof(_answer) );
    _answerIndex            = 0;
    _method                 = 0;
    _command                = 0;
    _moduleIndex            = 255;
    _moduleValue            = 9999;
    _moduleSelected         = false;
    _outputBufferIndex      = 0;
    _urlEndVerified         = false;
    _answerIsError          = false;
}
    
void ModuleRestApi2::addToBuffer(int toAdd)
{
    char number[10];
    itoa(toAdd,number,10);
        
    addToBuffer(number);
}
    
void ModuleRestApi2::addToBuffer( char* toAdd )
{
    //Serial.print(F("Added to buffer: "));Serial.println( toAdd );
    //Serial.print(F("ModuleRestApi: Added bytes: "));Serial.println( strlen(toAdd) );
        
    for( int i = 0; i < strlen( toAdd ); i++ )
        _outputBuffer[_outputBufferIndex+i] = toAdd[i];
        
    _outputBufferIndex += strlen( toAdd );
    //Serial.print(F("ModuleRestApi: After add used buffer: "));Serial.println( _outputBufferIndex );
}
    
void ModuleRestApi2::addToBuffer( const __FlashStringHelper* toAdd )
{
    // Serial.print(F("Added to buffer: "));
    // Serial.println(toAdd);
        
    uint8_t idx = 0;
        
    const char PROGMEM *p = (const char PROGMEM *)toAdd;
    while( true ) {
        unsigned char c = pgm_read_byte( p++ );
        if( c == 0 )
            break;
                
        _outputBuffer[_outputBufferIndex + idx] = c;
        idx++;
    }

    _outputBufferIndex += idx;
    
    //Serial.print(F("ModuleRestApi: After add used buffer: "));Serial.println( _outputBufferIndex );
}
    
uint16_t ModuleRestApi2::getIntFromAsciiAnswer( void )
{
    uint8_t number[4] = {0,0,0,0}; // Only pick 4 numbers (max valid value 1023)
    uint16_t value = 0;
    uint8_t numberCount = 0;

    // Todo improve this you lazy copy paste guy
    if( _answer[0] > 47 && _answer[0] < 58 ) {
        numberCount++;
        if( _answer[1] > 47 && _answer[1] < 58 ) {
            numberCount++;
            if( _answer[2] > 47 && _answer[2] < 58 ) {
                numberCount++;
                if( _answer[3] > 47 && _answer[3] < 58 )
                    numberCount++;
            }
        }
    }
        
    if( !numberCount )
        return 9999;
        
    for( uint8_t i=0; i<numberCount; i++ )
        number[i] = _answer[i];
        
    return atoi( (char*)number );
}
    
