
#include "BaseModule.h"

BaseModule::BaseModule( EepromHelper* eepromHelper, uint8_t eepromIndex, uint8_t pin, uint8_t pinType )
{
    _eepromHelper       = eepromHelper;
    _eepromIndex        = eepromIndex;
    _eepromUsage        = 0;
    _pin                = pin;
    _pinType            = pinType;
    _initialized        = false;
    _readable           = false;
    _writable           = false;
    _eepromSaveable     = false;
    _currentValue       = 0;
    _jsonChuckCounter   = 0;
    
    strncpy( _moduleType, "---", 4 );
}

uint8_t BaseModule::isInitialized()
{
    return _initialized;
}


void BaseModule::getModuleType( char moduleType[4] )
{
    strncpy( moduleType, _moduleType, 4 );
}

uint8_t BaseModule::isEepromSaveable()
{
    return _eepromSaveable;
}

uint8_t BaseModule::getPin()
{
    return _pin;
}

uint8_t BaseModule::getPinType()
{
    return _pinType;
}

uint8_t BaseModule::write( uint16_t value )
{
    if( !_writable || !_initialized )
        return false;
    
#ifdef MODULE_BASE_DEBUG
    Serial.print("BaseModule: Writing value: "); Serial.println(value);
#endif
    
    if( _pinType == 'd' ) { // Todo use defines for Digital and Analog pin types
        if( value != 0 && value != 1 )
            return false;

#ifdef MODULE_BASE_DEBUG
        Serial.print("BaseModule: Writing to pin: ");
        Serial.println(_pin);
#endif
        
        digitalWrite( _pin, value );
    }
    else {
        if( value > 1023 )
            return false;

#ifdef MODULE_BASE_DEBUG
        Serial.print("BaseModule: Writing to pin: ");
        Serial.println(_pin);
#endif

        analogWrite( _pin, value );
    }
    
    _currentValue = value;

#ifdef MODULE_BASE_DEBUG
    Serial.println("BaseModule: Success");
#endif
    
    return true;
}

 int16_t BaseModule::read()
{
    if( !_readable || !_initialized )
        return -1;
     
    if( _pinType == 'd' )
        _currentValue = digitalRead( _pin );
    else
        _currentValue = analogRead( _pin );
        
    return _currentValue;
}

/************************************************************************/
/*                      RESTFUL VIRTUAL METHODS                         */
/************************************************************************/

uint8_t BaseModule::restfulCreate( char buffer[MODULE_BUFFER_SIZE] )
{
    memset( &buffer[0], 0, MODULE_BUFFER_SIZE );
    strcpy( buffer, "Method not supported" );
    return false;
}

uint8_t BaseModule::restfulUpdate( char buffer[MODULE_BUFFER_SIZE] )
{
    memset( &buffer[0], 0, MODULE_BUFFER_SIZE );
    strcpy( buffer, "Method not supported" );
    return false;
}

uint8_t BaseModule::restfulDelete( char buffer[MODULE_BUFFER_SIZE] )
{
    memset( &buffer[0], 0, MODULE_BUFFER_SIZE );
    strcpy( buffer, "Method not supported" );
    return false;
}


/************************************************************************/
/*                          PROTECTED METHODS                           */
/************************************************************************/

/**
 * Adds the module id to the buffer. Common for every module.
 *
 * Format:
 *  {"id":1
 */
uint8_t BaseModule::addModuleJsonIdToBuffer( char buffer[] )
{
    uint8_t bufferPtr = 0;
    strncpy( buffer, "{\"id\":", 6 );
    bufferPtr += 6;
    
    // Fast way as long as there are less than 10 modules
    buffer[bufferPtr++] = _eepromIndex + 49;
    
    return bufferPtr;
}

/**
 * Supported values:
 *  - digital: "0"-"1"
 *  - analog: "0"-"1023"
 */
int16_t BaseModule::validateValue( const char* value )
{
    uint8_t valueLength = strlen( value );
    
    if( _pinType == 'd' ) {
        if( valueLength == 1 ) {
            if( value[0] == '0' || value[0] == '1' )
                return value[0]-48;
        }
    }
    else if( _pinType == 'a' ) {
        for( uint8_t i=0; i<valueLength; i++ ) {
            if( value[i] < '0' || value[i] > '9' ) {
#ifdef MODULE_BASE_DEBUG
                Serial.print("BaseModule: 1 Validate value failed: "); Serial.println(value);
#endif
                return -1;
            }
        }
        
        int intValue = atoi( value );
        
        if( intValue >= 0 && intValue <= 1023 )
            return (int16_t)intValue;
    }

#ifdef MODULE_BASE_DEBUG
    Serial.print("BaseModule: 2 Validate value failed: ");
    Serial.println(value);
#endif
    
    return -1;
}
