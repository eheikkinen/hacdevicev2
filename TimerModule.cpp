#include "TimerModule.h"

TimerModule::TimerModule( EepromHelper* eepromHelper, uint8_t eepromIndex, uint8_t pin, uint8_t pinType ) :
	BaseModule( eepromHelper, eepromIndex, pin, pinType )
{
}

/**
 * Initialize 3 steps:
 * - Pins
 * - The four variables
 * - Initial values
 */
void TimerModule::initialize()
{
    // Tell Eeprom memory usage
    _eepromHelper->moduleMemoryUsage( _eepromIndex, TIMER_EEPROM_SIZE );

    // Pins
    // -

    // The four variables
    _readable       = true;
    _writable       = true;
    _eepromSaveable = true;
    strncpy( _moduleType, MODULE_TIMER, 4 );
    memset( _timersData, -1, sizeof( _timersData ) );

    // Values
    initializeTimersData();
    _hh = 0;
    _mm = 0;
    _ss = 0;

    // Done
    _initialized	= true;

    if( MODULE_TIMER_DEBUG )
        Serial.println(F("Timer module initialized"));
}

/**
 * Reads the timer module status in json format to the buffer
 *
 * Format:
 *  {"id":1',"type":"tim","value":"18:30:23","timers":2}
 */
uint8_t TimerModule::readStatus( char buffer[MODULE_BUFFER_SIZE] )
{
    if( !_initialized )
        return false;

    memset( &buffer[0], 0, MODULE_BUFFER_SIZE );

    uint8_t bufferPtr = 0;
    char number[5];

    // Add id: '{"id":1' (note no comma in end)
    bufferPtr = addModuleJsonIdToBuffer( buffer );

    // Add type
    strncpy( &buffer[bufferPtr], ",\"type\":\"", 9 );
    bufferPtr += 9;
    strncpy( &buffer[bufferPtr], _moduleType, 3 );
    bufferPtr += 3;

    // Add value
    strncpy( &buffer[bufferPtr], "\",\"value\":\"", 11 );
    bufferPtr += 11;

    getTimeString( (uint8_t*)&buffer[bufferPtr] );
    bufferPtr += 8; // Adds string: "18:30:23"

    // Add timers count
    strncpy( &buffer[bufferPtr], "\",\"timers\":", 11 );
    bufferPtr += 11;

    itoa( _setTimerCount, number, 10 );
    for( uint8_t i=0; i< strlen( number ); i++ )
        buffer[bufferPtr++] = number[i];

    buffer[bufferPtr++] = '}';

#ifdef MODULE_TIMER_DEBUG
    Serial.print("TimerModule: latest status added bytes ");
    Serial.println(bufferPtr);
#endif

    return bufferPtr;
}

void TimerModule::handleSecondPoll( DateTime* now )
{
    if( !_initialized )
        return;

    // Update time
    _hh = now->hour();
    _mm = now->minute();
    _ss = now->second();

    // Check for timer matches
    int16_t value = compare( now );
    if( value != -1 ) {
#ifdef MODULE_TIMER_DEBUG
        Serial.println("Found compare match: Writing value");
#endif
        write( value );
    }
}

void TimerModule::getTimeString( uint8_t time[] )
{
    char number[3];
    uint8_t ptr = 0;

    // Hours
    memset( &number[0], 0, 3 );
    itoa( _hh, number, 10 );
    if( strlen( number ) == 1 ) {
        time[ptr++] = '0';
        time[ptr++] = number[0];
    } else {
        time[ptr++] = number[0];
        time[ptr++] = number[1];
    }
    time[ptr++] = ':';

    // Minutes
    memset( &number[0], 0, 3 );
    itoa( _mm, number, 10 );
    if( strlen( number ) == 1 ) {
        time[ptr++] = '0';
        time[ptr++] = number[0];
    } else {
        time[ptr++] = number[0];
        time[ptr++] = number[1];
    }
    time[ptr++] = ':';

    // Seconds
    memset( &number[0], 0, 3 );
    itoa( _ss, number, 10 );
    if( strlen( number ) == 1 ) {
        time[ptr++] = '0';
        time[ptr++] = number[0];
    } else {
        time[ptr++] = number[0];
        time[ptr++] = number[1];
    }
}

/************************************************************************/
/*                          RESTFUL METHODS                             */
/************************************************************************/

/**
 * Create new timer
 *
 * Payload:
 *   { "hh":"12", "mm":"30", "ss":"00", "value":"1" }
 */
uint8_t TimerModule::restfulCreate( char buffer[MODULE_BUFFER_SIZE] )
{
#ifdef MODULE_TIMER_DEBUG
    Serial.println(F("TimerModule: Adding new"));
#endif

    StaticJsonBuffer<MODULE_BUFFER_SIZE> jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject( buffer );

    if( !root.success() ) {
        memset( &buffer[0], 0, MODULE_BUFFER_SIZE );
        strcpy( buffer, "Invalid json" );
        return false;
    }

    int8_t hh = validateHours( root["hh"] );
    if( hh == -1 ) {
        memset( &buffer[0], 0, MODULE_BUFFER_SIZE );
        strcpy( buffer, "Invalid hh given" );
        return false;
    }

    int8_t mm = validateMinutesAndSeconds( root["mm"] );
    if( mm == -1 ) {
        memset( &buffer[0], 0, MODULE_BUFFER_SIZE );
        strcpy( buffer, "Invalid mm given" );
        return false;
    }

    int8_t ss = validateMinutesAndSeconds( root["ss"] );
    if( ss == -1 ) {
        memset( &buffer[0], 0, MODULE_BUFFER_SIZE );
        strcpy( buffer, "Invalid ss given" );
        return false;
    }

    int16_t value = validateValue( root["value"] );
    if( value == -1 ) {
        memset( &buffer[0], 0, MODULE_BUFFER_SIZE );
        strcpy( buffer, "Invalid value given" );
        return false;
    }

    uint8_t index = saveTimerRow( hh, mm, ss, value, -1 );
    if( !index ) {
        memset( &buffer[0], 0, MODULE_BUFFER_SIZE );
        strcpy( buffer, "Timers full" );
        return false;
    }

    // Return the ID (index+1)
    return index+1;
}

/**
 * Read all timers to buffer in json format (todo make more clear / split)
 */
uint8_t TimerModule::restfulRead( char buffer[MODULE_BUFFER_SIZE] ) 
{
    if( !_initialized )
        return false;

    uint8_t bufferPtr = 0;
    memset( &buffer[0], 0, MODULE_BUFFER_SIZE );

    // First MODULE_BUFFER_SIZE bytes buffer gives the start
    if( _jsonChuckCounter == 0 )
    {
        // Add type
        strncpy( &buffer[bufferPtr], ",\"type\":\"", 9 );
        bufferPtr += 9;
        strncpy( &buffer[bufferPtr], _moduleType, 3 );
        bufferPtr += 3;

        // Add value
        strncpy( &buffer[bufferPtr], "\",\"value\":\"", 11 );
        bufferPtr += 11;

        getTimeString( (uint8_t*)&buffer[bufferPtr] );
        bufferPtr += 8; // Adds string: "18:30:23"

        strcpy( &buffer[bufferPtr], "\",\"timers\":[" );
        bufferPtr += 12;

        if( _setTimerCount == 0 )
            buffer[bufferPtr++] = ']';

        _jsonChuckCounter = 1;

#ifdef MODULE_TIMER_DEBUG
        Serial.print("TimerModule: Start added bytes: ");
        Serial.println(bufferPtr);
#endif

        return true;
    }
    else {
        if( _jsonChuckCounter <= _setTimerCount ) {
            // Figure the index that the data is (maybe not in order)
            uint8_t timerIndex      = -1;
            uint8_t ignoreCounter   = 0;
            for( uint8_t i=0; i<10; i++ ) {
                if( _timerIndexSet[i] ) {
                    ignoreCounter++;
                    if( ignoreCounter == _jsonChuckCounter ) {
                        timerIndex = i;
                        break;
                    }
                }
            }

            // Should not be possible, just a safety net
            if( timerIndex == -1 ) {
                _jsonChuckCounter = 0;
                return false;
            }

            uint8_t ptr = 0 + (timerIndex * TIMER_EEPROM_SINGLE_ROW);

            if( _jsonChuckCounter != 1 )
                buffer[bufferPtr++] = ',';

            char number[5];

            //////////// Add Id ////////////
            strncpy( &buffer[bufferPtr], "{\"id\":", 6 );
            bufferPtr += 6;

            memset( &number[0], 0, 5 );
            itoa( timerIndex+1, number, 10 );
            for( uint8_t j=0; j<strlen( number ); j++ )
                buffer[bufferPtr++] = number[j];


            //////////// Add Time ////////////
            strncpy( &buffer[bufferPtr], ",\"time\":\"", 9 );
            bufferPtr += 9;

            uint8_t hh          = _timersData[ptr++];
            uint8_t mm          = _timersData[ptr++];
            uint8_t ss          = _timersData[ptr++];
            uint8_t valueHigh   = _timersData[ptr++];
            uint8_t valueLow    = _timersData[ptr++];
            uint16_t value      = (valueHigh << 8) + valueLow;

            // Add hours
            if( hh == TIMER_WILDCARD ) {
                buffer[bufferPtr++] = '*';
                buffer[bufferPtr++] = '*';
            } else {
                memset( &number[0], 0, 5 );
                itoa( hh, number, 10 );
                if( strlen(number) == 1 )
                    buffer[bufferPtr++] = '0';
                for( uint8_t j=0; j<strlen( number ); j++ )
                    buffer[bufferPtr++] = number[j];
            }

            buffer[bufferPtr++] = ':';

            // Add minutes
            if( mm == TIMER_WILDCARD ) {
                buffer[bufferPtr++] = '*';
                buffer[bufferPtr++] = '*';
            } else {
                memset( &number[0], 0, 5 );
                itoa( mm, number, 10 );
                if( strlen(number) == 1 )
                        buffer[bufferPtr++] = '0';
                for( uint8_t j=0; j<strlen( number ); j++ )
                        buffer[bufferPtr++] = number[j];
            }

            buffer[bufferPtr++] = ':';

            // Add seconds
            if( ss == TIMER_WILDCARD ) {
                buffer[bufferPtr++] = '*';
                buffer[bufferPtr++] = '*';
            } else {
                memset( &number[0], 0, 5 );
                itoa( ss, number, 10 );
                if( strlen(number) == 1 )
                    buffer[bufferPtr++] = '0';
                for( uint8_t j=0; j<strlen( number ); j++ )
                    buffer[bufferPtr++] = number[j];
            }

            //////////// Add Value ////////////
            strncpy( &buffer[bufferPtr], "\",\"value\":", 10 );
            bufferPtr += 10;

            memset( &number[0], 0, 5 );
            itoa( value, number, 10 );
            for( uint8_t j=0; j<strlen( number ); j++ )
                 buffer[bufferPtr++] = number[j];

            buffer[bufferPtr++] = '}';

            _jsonChuckCounter++;

            // For the last row, add the ]
            if( _jsonChuckCounter > _setTimerCount )
                buffer[bufferPtr++] = ']';

#ifdef MODULE_TIMER_DEBUG
            Serial.print("TimerModule: Added bytes ");
            Serial.println(bufferPtr);
#endif

            return true;
        }
    }

    //Serial.println("TimerModule: last loop executed ");

    _jsonChuckCounter = 0;

    return false;
}

/**
 * Update existing timer
 *
 * Payload:
 *   { "id":"1","hh":"10","mm":"30","ss":15","value":"1" }
 */
uint8_t TimerModule::restfulUpdate( char buffer[MODULE_BUFFER_SIZE] )
{
    StaticJsonBuffer<MODULE_BUFFER_SIZE> jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject( buffer );

    if( !root.success() ) {
        memset( &buffer[0], 0, MODULE_BUFFER_SIZE );
        strcpy( buffer, "Invalid json" );
        return false;
    }

    int8_t id = validateId( root["id"] );
    if( id == -1 ) {
        memset( &buffer[0], 0, MODULE_BUFFER_SIZE );
        strcpy( buffer, "Invalid id given" );
        return false;
    }

    int8_t hh = validateHours( root["hh"] );
    if( hh == -1 )
    {
        memset( &buffer[0], 0, MODULE_BUFFER_SIZE );
        strcpy( buffer, "Invalid hh given" );
        return false;
    }

    int8_t mm = validateMinutesAndSeconds( root["mm"] );
    if( mm == -1 ) {
        memset( &buffer[0], 0, MODULE_BUFFER_SIZE );
        strcpy( buffer, "Invalid mm given" );
        return false;
    }

    int8_t ss = validateMinutesAndSeconds( root["ss"] );
    if( ss == -1 ) {
        memset( &buffer[0], 0, MODULE_BUFFER_SIZE );
        strcpy( buffer, "Invalid ss given" );
        return false;
    }

    int16_t value = validateValue( root["value"] );
    if( value == -1 ) {
        memset( &buffer[0], 0, MODULE_BUFFER_SIZE );
        strcpy( buffer, "Invalid value given" );
        return false;
    }

    // Save the timer data, over writes if row exists
    saveTimerRow( hh, mm, ss, value, id-1 );

    return true;
}

/**
 * Delete existing timer
 *
 * Payload:
 *   { "id":"1" }
 */
uint8_t TimerModule::restfulDelete( char buffer[MODULE_BUFFER_SIZE] )
{
    StaticJsonBuffer<MODULE_BUFFER_SIZE> jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject( buffer );

    if( !root.success() ) {
        memset( &buffer[0], 0, MODULE_BUFFER_SIZE );
        strcpy( buffer, "Invalid json" );
        return false;
    }

    // Passes even if the id data row is not set
    int8_t id = validateId( root["id"] );
    if( id == -1 ) {
        memset( &buffer[0], 0, MODULE_BUFFER_SIZE );
        strcpy( buffer, "Invalid id given" );
        return false;
    }

    deleteTimerRow( id-1 );

    if( _timerIndexSet[id-1] ) {
        _setTimerCount--;
        _timerIndexSet[id-1] = false;
    }

    return true;
}


/************************************************************************/
/*                         Private methods                              */
/************************************************************************/

int16_t TimerModule::compare( DateTime* dt )
{
    uint8_t ptr = 0;

#ifdef MODULE_TIMER_DEBUG
    Serial.print(dt->hour());
    Serial.print(":");
    Serial.print(dt->minute());
    Serial.print(":");
    Serial.println(dt->second());
#endif

    for( uint8_t i=0; i<TIMER_ROW_COUNT; i++ ) {
        if( !_timerIndexSet[i] ) {
            ptr += TIMER_EEPROM_SINGLE_ROW;
            continue;
        }

        uint8_t hh          = _timersData[ptr++];
        uint8_t mm          = _timersData[ptr++];
        uint8_t ss          = _timersData[ptr++];
        uint8_t valueHigh   = _timersData[ptr++];
        uint8_t valueLow    = _timersData[ptr++];
        uint16_t value      = (valueHigh << 8) + valueLow;

        // Supports wild cards '@' = any
        if( ( hh == TIMER_WILDCARD || hh == dt->hour() ) && ( mm == TIMER_WILDCARD || mm == dt->minute() ) && ( ss == TIMER_WILDCARD || ss == dt->second() ) )
            return value;
    }

    return -1;
}

void TimerModule::initializeTimersData()
{
    // Load data from EEPROM
    _eepromHelper->getEepromModuleData( _eepromIndex, _timersData );

    uint8_t ptr = 0;
    for( uint8_t i=0; i<10; i++ )
    {
        uint8_t hh          = _timersData[ptr++];
        uint8_t mm          = _timersData[ptr++];
        uint8_t ss          = _timersData[ptr++];
        uint8_t valueHigh   = _timersData[ptr++];
        uint8_t valueLow    = _timersData[ptr++];

        if( ( hh == TIMER_WILDCARD || hh <= 23 && hh >= 0 ) &&
                ( mm == TIMER_WILDCARD || mm >= 0 && mm <= 59 ) &&
                ( ss == TIMER_WILDCARD || ss >= 0 && ss <= 59 ) ) {
            _timerIndexSet[i] = true;
            _setTimerCount++;
            //Serial.print("Timer data row is set at index"); Serial.println(i);
        } else {
            _timerIndexSet[i] = false;
            //Serial.print("Timer data row is NOT set at index"); Serial.println(i);
        }
    }
}

uint8_t TimerModule::saveTimerRow( uint8_t hh, uint8_t mm, uint8_t ss, int16_t value, int8_t index )
{
    if( index == -1 ) {
        // Find first free
        for( uint8_t i=0; i<TIMER_ROW_COUNT; i++ ) {
            if( !_timerIndexSet[i] ) {
                index = i;
                break;
            }
        }
    }

    if( index == -1 )
        return false;

    uint8_t startAddress = index * TIMER_EEPROM_SINGLE_ROW;

    _timersData[startAddress++] = hh;
    _timersData[startAddress++] = mm;
    _timersData[startAddress++] = ss;
    _timersData[startAddress++] = (uint8_t)(value >> 8);
    _timersData[startAddress++] = (uint8_t)value;

    if( !_timerIndexSet[index] ) {
        _timerIndexSet[index] = true;
        _setTimerCount++;
    }

    _eepromHelper->saveEepromModuleData( _eepromIndex, _timersData );

#ifdef MODULE_TIMER_DEBUG
    Serial.println(F("TimerModule: Save wrote timer module data to eeprom"));
#endif

    return index;
}

uint8_t TimerModule::deleteTimerRow( uint8_t index )
{
    if( !_timerIndexSet[index] )
        return true;

    uint8_t startAddress = index * TIMER_EEPROM_SINGLE_ROW;
    for( uint8_t i=0; i<TIMER_EEPROM_SINGLE_ROW; i++ )
        _timersData[startAddress++] = 0xFF;

    _eepromHelper->saveEepromModuleData( _eepromIndex, _timersData );

#ifdef MODULE_TIMER_DEBUG
    Serial.println(F("TimerModule: Delete wrote timer module data to eeprom"));
#endif
}

/**
 * Supported values "*", "**" and "0"-"23"
 */
int8_t TimerModule::validateHours( const char* hh )
{
    uint8_t hhLength = strlen( hh );
    if( hhLength == 1 ) {
        if( hh[0] >= '0' && hh[0] <= '9' )
            return hh[0]-48;

        if( hh[0] == '*' )
            return TIMER_WILDCARD;
    }
    else if( hhLength == 2 ) {
        if( hh[0] == '*' && hh[1] == '*' )
            return TIMER_WILDCARD;

        for( uint8_t i=0; i<hhLength; i++ ) {
            if( hh[i] < '0' || hh[i] > '9' ) {
#ifdef MODULE_TIMER_DEBUG
                Serial.print("TimerModule: 1 Validate hh failed: ");
                Serial.println(hh);
#endif
                return -1;
            }
        }

        int intValue = atoi( hh );

        if( intValue >= 0 && intValue <= 23 )
                return intValue;
    }

#ifdef MODULE_TIMER_DEBUG
    Serial.print("TimerModule: 2 Validate hh failed: ");
    Serial.println(hh);
#endif

    return -1;
}

/**
 * Supported values "*", "**" and "0"-"59"
 */
int8_t TimerModule::validateMinutesAndSeconds( const char* ss )
{
    uint8_t ssLength = strlen( ss );

    if( ssLength == 1 ) {
        if( ss[0] >= '0' && ss[0] <= '9' )
            return ss[0]-48;

        if( ss[0] == '*' )
            return TIMER_WILDCARD;
    }
    else if( ssLength == 2 ) {
        if( ss[0] == '*' && ss[1] == '*' )
            return TIMER_WILDCARD;

        for( uint8_t i=0; i<ssLength; i++ ) {
            if( ss[i] < '0' || ss[i] > '9' ) {
#ifdef MODULE_TIMER_DEBUG
                Serial.print("TimerModule: 1 Validate ss failed: ");
                Serial.println(ss);
#endif
                return -1;
            }
        }

        int intValue = atoi( ss );
        if( intValue >= 0 && intValue <= 59 )
            return (int8_t)intValue;
    }

#ifdef MODULE_TIMER_DEBUG
    Serial.print("TimerModule: 2 Validate mins and secs failed: ");
    Serial.println(ss);
#endif

    return -1;
}

/**
 * Supported values "1"-"10"
 */
int8_t TimerModule::validateId( const char* id )
{
    uint8_t idLength = strlen( id );

    if( idLength == 1 ) {
        if( id[0] >= '1' && id[0] <= '9' )
            return id[0]-48;
    }
    else if( idLength == 2 ) {
        for( uint8_t i=0; i<idLength; i++ ) {
            if( id[i] < '0' || id[i] > '9' ) {
#ifdef MODULE_TIMER_DEBUG
                Serial.print("TimerModule: Validate id failed: ");
                Serial.println(id);
#endif
                return -1;
            }
        }

        int intValue = atoi( id );

        if( intValue >= 1 && intValue <= 10 )
                return (int8_t)intValue;
    }

#ifdef MODULE_TIMER_DEBUG
    Serial.print("TimerModule: Validate id failed: ");
    Serial.println(id);
#endif

    return -1;
}

#ifdef MODULE_TIMER_DEBUG
void TimerModule::dumpCurrentTimerDatas()
{
    uint8_t ptr = 0;
    Serial.println(" ---DUMP TIMER DATA START --- ");

    for( uint8_t i=0; i<TIMER_ROW_COUNT; i++ )
    {
        if( !_timerIndexSet[i] ) {
            ptr += TIMER_EEPROM_SINGLE_ROW;
            continue;
        }

        uint8_t h           = _timersData[ptr++];
        uint8_t m           = _timersData[ptr++];
        uint8_t s           = _timersData[ptr++];
        uint8_t valueHigh   = _timersData[ptr++];
        uint8_t valueLow    = _timersData[ptr++];
        uint16_t value      = (valueHigh << 8) + valueLow;

        Serial.print(i);
        Serial.print(": ");
        Serial.print(h);
        Serial.print(":");
        Serial.print(m);
        Serial.print(":");
        Serial.print(s);
        Serial.print(" - ");
        Serial.println(value);
    }

    Serial.println(" --- DUMP TIMER DATA END --- ");
}
#endif
