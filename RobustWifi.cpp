#include "RobustWifi.h"

/************************************************************************/
/*                          PUBLIC METHODS                              */
/************************************************************************/

RobustWifi::RobustWifi( EepromHelper* eepromHelper, Adafruit_CC3000* cc3000, Adafruit_CC3000_Server* server, MDNSResponder* mdns, ModuleRestApi2* moduleRestApi, Device* device )
{
    _eepromHelper   = eepromHelper;
    _cc3000         = cc3000;
    _server         = server;
    _mdns           = mdns;
    _moduleRestApi  = moduleRestApi;
    _device         = device;
    
    _pushButtonPressed = false;
    
    memset( _ssid, 0, sizeof( _ssid ) );
}

void RobustWifi::initialize()
{
    _device->initiateBlinkRate();
    
    if( _device->isDeviceNameSet() )
        bootupOpenWifiUsingSmartConfs();
    else {
        getDeviceNameFromMacAddress();
        bootupRestartWifiSmartConfig();
    }
    
    if( !_mdns->begin( _device->getDeviceNamePtr(), *_cc3000 ) ) {
        Serial.println(F("RobustWifi: Error setting up MDNS responder!"));
        while(1);
    }
    
    getConnectionDetails();
    
    updateWifiRssi();
    
    _server->begin();
    
    _device->setBlinkRate( BLINK_RATE_ON );
}

void RobustWifi::handle()
{
    // If push button was pressed, release current
    // SmartConfig and poll for new ones
    if( _pushButtonPressed ) {
#ifdef ROBUST_WIFI_DEBUG
        Serial.println(F("Restarting smart config"));
#endif
            
        bootupRestartWifiSmartConfig();
        _pushButtonPressed = false;
    }
    
    if( !_cc3000->checkConnected() )
        retryOpenWifi();
    
    _mdns->update();
    
    Adafruit_CC3000_ClientRef client = _server->available();
    _moduleRestApi->handle( client );
}

void RobustWifi::pushButtonPressed()
{
    _pushButtonPressed = true;
}

uint8_t RobustWifi::getBlinkRate()
{
    return _blinkRate;
}

void RobustWifi::handleReconnectIfNothingReceived()
{
    if( !_moduleRestApi->getPacketReceived() ) {
#ifdef ROBUST_WIFI_DEBUG
        Serial.println("RobustWifi: No packet was received in the period");
#endif
            
        retryOpenWifi();
    }
}

void RobustWifi::addModule( BaseModule* module )
{
    if( !module->isInitialized() )
        Serial.println("RobustWifi: WARNING: Adding uninitialized module");
    
    _moduleRestApi->addModule( module );
}

void RobustWifi::addModules( BaseModule* modules[MODULE_COUNT] )
{
    for( uint8_t i=0; i<MODULE_COUNT; i++ ) {
        if( !modules[i]->isInitialized() )
            Serial.println("RobustWifi: WARNING: Adding uninitialized module");
    }
    _moduleRestApi->addModules( modules );
}


void RobustWifi::updateWifiRssi( void )
{
    uint32_t index;
    uint8_t rssi, sec;
    char ssidname[33];

    if( !_cc3000->startSSIDscan( &index, 1000 ) )
        return;
    
    while( index-- ) {
        _cc3000->getNextSSID( &rssi, &sec, ssidname );
        
        if( strcmp( ssidname, _ssid ) == 0 ) {
            _device->setRssi( (uint8_t)((uint16_t)(rssi * 100) / RSSI_MAX_VALUE ) );
            break;
        }
    }
    
    _cc3000->stopSSIDscan();
}


/************************************************************************/
/*                         PRIVATE METHODS                              */
/************************************************************************/

void RobustWifi::bootupOpenWifiUsingSmartConfs()
{
    if( ROBUST_WIFI_DEBUG )
        Serial.println(F("RobustWifi: booting up using smart config"));
    
    _device->setBlinkRate( BLINK_RATE_1HZ );
  
    // Connect to previously setted SmartConfigs
    if( !_cc3000->begin( false, true, _device->getDeviceNamePtr() ) ) { // This can only be called once!
        Serial.println(F("RobustWifi: Connection failed"));
      
        // If push button was pressed, force SmartConfig reset
        if( _pushButtonPressed ) {
#ifdef ROBUST_WIFI_DEBUG
            Serial.println(F("RobustWifi: Push button was pressed, reseting SmartConfig"));
#endif
            
            bootupRestartWifiSmartConfig();
            _pushButtonPressed = false;
            return;
        }
      
        // Returns true if SmartConfig eventually worked -> require checkDHCP() calls!
        // Retrusn false if user pressed button and gave new SmartConfig -> skip checkDHCP() calls!
        if( !retryOpenWifiUsingSmartConfig() ) {
#ifdef ROBUST_WIFI_DEBUG
            Serial.println(F("RobustWifi: Connection succeeded"));
#endif

            _device->setBlinkRate( BLINK_RATE_ON );
            return;
        }
    }

#ifdef ROBUST_WIFI_DEBUG
    Serial.println(F("RobustWifi: Connection succeeded"));
#endif
  
    while( !_cc3000->checkDHCP() )
        delay(100); // ToDo: Insert a DHCP timeout!
    
    _device->setBlinkRate( BLINK_RATE_ON );
}

/**
 * Tries to open the connection using the previously
 * given SmartConfig until connection succeedes or 
 * the user presses button and gives new SmartConfig.
 *
 * Returns: 
 *  True if checkDHCP() is required to be executed after
 *  False if checkDHCP() is not required anymore
 */
uint8_t RobustWifi::retryOpenWifiUsingSmartConfig()
{
#ifdef ROBUST_WIFI_DEBUG
    Serial.println(F("RobustWifi: Retrying to connect using SmartConfig"));
#endif
  
    // Wifi connection has already been initialized
    while( !_cc3000->retrySmartConfig( _device->getDeviceNamePtr() ) ) {
#ifdef ROBUST_WIFI_DEBUG
        Serial.println(F("RobustWifi: Connection failed"));
#endif
    
        // If push button was pressed, force SmartConfig reset
        if( _pushButtonPressed ) {
#ifdef ROBUST_WIFI_DEBUG
            Serial.println(F("RobustWifi: Push button was pressed, reseting SmartConfig"));
#endif
            
            bootupRestartWifiSmartConfig();
            _pushButtonPressed = false;
            return false;
        }
    }

#ifdef ROBUST_WIFI_DEBUG
    Serial.println(F("RobustWifi: Connection succeeded"));
#endif
        
    return true;
}


void RobustWifi::bootupRestartWifiSmartConfig()
{
    _device->setBlinkRate( BLINK_RATE_2HZ );
    
    // Clear all the SmartConfig profiles
    _cc3000->deleteProfiles();

#ifdef ROBUST_WIFI_DEBUG
    Serial.print(F("RobustWifi: Waiting for SmartConfig (~60) ("));
    Serial.print( _device->getDeviceNamePtr() );
    Serial.println(F(")"));
#endif
    
    while( !_cc3000->startSmartConfig( _device->getDeviceNamePtr() ) ) {
#ifdef ROBUST_WIFI_DEBUG
        Serial.println(F("RobustWifi: SmartConfig failed, retrying"));
#endif
    }

#ifdef ROBUST_WIFI_DEBUG
    Serial.println(F("RobustWifi: Succesfully got new SmartConfig"));
#endif
    
    while( !_cc3000->checkDHCP() )
        delay(100); // ToDo: Insert a DHCP timeout!
    
    _device->setBlinkRate( BLINK_RATE_ON );
}

void RobustWifi::retryOpenWifi( void )
{
#ifdef ROBUST_WIFI_DEBUG
    Serial.println(F("RobustWifi: Disconnected while executing app"));
#endif
    
    _device->setBlinkRate( BLINK_RATE_1HZ ); // 0,5sec ON, 0,5sec OFF
    
    _cc3000->disconnect();
    delay(100);
    _cc3000->reboot(); // Reduced delay inside from 5000 -> 100
    
    // Stay here until new connection is made
    if( retryOpenWifiUsingSmartConfig() ) {
        while( !_cc3000->checkDHCP() )
            delay(100); // ToDo: Insert a DHCP timeout!
    }
    
    // Wifi is now connected (one way or other)
    if( !_mdns->begin( _device->getDeviceNamePtr(), *_cc3000 ) ) {
        Serial.println(F("RobustWifi: Error setting up MDNS responder!"));
        while(1);
    }
    
    _server->begin();

#ifdef ROBUST_WIFI_DEBUG
    Serial.println(F("RobustWifi: New connection was made!"));
#endif
    
    _device->setBlinkRate( BLINK_RATE_ON );
}

void RobustWifi::getDeviceNameFromMacAddress()
{
    // Need to figure out the CC3000 mac address
    // to generate the unique device name
#ifdef ROBUST_WIFI_DEBUG
    Serial.println( "RobustWifi: Device name has not been set in EEPROM" );
#endif
    
    if( !_cc3000->begin() ) {
        Serial.println( "RobustWifi: ERROR: CC3000 begin failed");
        while(1); // Something wrong with the CC3000! SERIOUS PROBLEM!
    }
    
    uint8_t macAddress[6]; 
    if( !_cc3000->getMacAddress( macAddress ) ) {
        Serial.println(F("RobustWifi: ERROR: Unable to retrieve MAC Address!\r\n"));
        while(1); // Something wrong with the CC3000! SERIOUS PROBLEM!
    }
    
    _device->setDeviceNameFromMacAddress( macAddress );

#ifdef ROBUST_WIFI_DEBUG
    Serial.println( "RobustWifi: Got name from mac address" );
#endif
}

void RobustWifi::getConnectionDetails()
{
    uint32_t tmp;
    _cc3000->getIPAddress( &tmp, &tmp, &tmp, &tmp, &tmp, (uint8_t*)_ssid );

#ifdef ROBUST_WIFI_DEBUG
    Serial.print("Ssid: ");
    Serial.println(_ssid);
#endif
}
